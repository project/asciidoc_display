<?php

/**
 * @file
 * Base module for importing/displaying AsciiDoc output in a Drupal site.
 */

/**
 * Implements hook_help().
 */
function asciidoc_display_help($path, $arg) {
  switch ($path) {
    case 'admin/help#asciidoc_display':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The AsciiDoc Display Base module contains common routines used by the AsciiDoc Display Direct and AsciiDoc Display Import modules. It does nothing on its own; you will need to also enable one of the other two modules.') . '</p>';

      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function asciidoc_display_menu() {
  // Administrative pages.
  $items['admin/config/development/asciidoc'] = array(
    'title' => 'AsciiDoc configuration',
    'description' => 'Configure AsciiDoc to be displayed in the site',
    'page callback' => 'asciidoc_display_main_admin',
    'file' => 'asciidoc_display.admin.inc',
    'access arguments' => array('administer asciidoc'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/development/asciidoc/main'] = array(
    'title' => 'AsciiDoc configuration',
    'description' => 'Configure AsciiDoc to be displayed in the site',
    'page callback' => 'asciidoc_display_main_admin',
    'file' => 'asciidoc_display.admin.inc',
    'access arguments' => array('administer asciidoc'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );


  $items['admin/config/development/asciidoc/clear_cache'] = array(
    'title' => 'Clear AsciiDoc book cache',
    'page callback' => 'asciidoc_display_clear_cache_page',
    'file' => 'asciidoc_display.admin.inc',
    'access arguments' => array('administer asciidoc'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function asciidoc_display_permission() {
  return array(
    'administer asciidoc' => array(
      'title' => t('Administer AsciiDoc'),
      'restrict access' => TRUE,
    ),
    'edit asciidoc source' => array(
      'title' => t('Use AsciiDoc source file editor'),
      'description' => t('Users will download edited files or diff files. They will not be saved or used in any way on the site.'),
    ),
  );
}

/**
 * Implements hook_library().
 */
function asciidoc_display_library() {
  $libs = array();

  $libs['asciidoc_display.edit'] = array(
    'title' => 'AsciiDoc Display Editing',
    'website' => 'https://drupal.org/project/asciidoc_display',
    'version' => '7.x-1.x',
    'js' => array(
      'sites/all/libraries/asciidoctor/dist/asciidoctor-all.min.js' => array('type' => 'file', 'group' => JS_LIBRARY),
      'sites/all/libraries/markitup/markitup/jquery.markitup.js' => array('type' => 'file', 'group' => JS_LIBRARY),
      drupal_get_path('module', 'asciidoc_display') .'/js/asciidoc_display.edit.js' => array('type' => 'file', 'group' => JS_LIBRARY),
    ),
    'css' => array(
      'sites/all/libraries/markitup/markitup/skins/markitup/style.css' => array('basename' => 'asciidoc_display/skins/style.css'),
      'sites/all/libraries/asciidoctor_images/asciidoc_display.edit.css' => array(),
    ),
    'dependencies' => array(
      array('system', 'jquery'),
    ),
  );

  return $libs;
}

/**
 * Decides whether the library for patches is present or not.
 *
 * @return bool
 *   TRUE if the patching library is present; FALSE if not.
 */
function asciidoc_display_patches_allowed() {
  return function_exists('xdiff_string_diff');
}

/**
 * Clears the page calculation caches for this module.
 */
function asciidoc_display_clear_calculation_caches() {
  cache_set('asciidoc_display_parse_bare_html', array());
  drupal_static_reset('asciidoc_display_parse_bare_html');
  cache_set('asciidoc_display_list_languages', array());
}
