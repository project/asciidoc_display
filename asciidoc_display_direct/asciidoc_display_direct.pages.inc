<?php

/**
 * @file
 * Page-generating functions for AsciiDoc direct display, including blocks.
 */

// Most of the functions in this file depend on functions in the main
// module's include file.
module_load_include('inc', 'asciidoc_display', 'asciidoc_display');

/**
 * Page callback: Displays an AsciiDoc page.
 */
function asciidoc_display_direct_build_page() {
  // If a problem occurs, we'll want to return a message.
  $error = array(
    '#type' => 'markup',
    '#markup' => '',
  );

  $arg = arg();
  $page_info = _asciidoc_display_direct_find_page($arg);

  if (isset($page_info['redirect']) && $page_info['redirect']) {
    drupal_goto($page_info['redirect']);
  }

  if (isset($page_info['image_file']) && $page_info['image_file']) {
    $path = drupal_realpath($page_info['image_file']);
    $handle = @fopen($path, 'rb');
    if (!$handle) {
      drupal_not_found();
    }

    drupal_add_http_header('Content-type', 'image');
    drupal_send_headers();
    // Transfer file in chunks, as in Drupal's file_transfer() function.
    // We cannot use that directly, because this is not in the standard
    // file system locations.
    while (!feof($handle)) {
      print fread($handle, 1024);
    }
    fclose($handle);
    drupal_exit();
  }

  if (!$page_info['book'] || $page_info['edit_file']) {
    $error['#markup'] = t('Could not load page');
    return $error;
  }

  $path = drupal_realpath($page_info['page_file']);
  $html = @file_get_contents($path);
  if (!$html) {
    $error['#markup'] = t('This page is currently empty.');
    return $error;
  }

  // Set the title based on the book/section title content.
  $html_info = asciidoc_display_parse_bare_html($html, array('keep_toc' => $page_info['is_index'], 'highlight_php' => TRUE));
  if ($html_info['book_title'] != $html_info['section_title']) {
    $html_info['section_title'] .= ' - ' . $html_info['book_title'];
  }
  drupal_set_title($html_info['section_title']);

  // Add the CSS.
  drupal_add_css(drupal_get_path('module', 'asciidoc_display_direct') . '/asciidoc-display.css');

  // Return the body for display on the page.
  if (empty($html_info['body'])) {
    $error['#markup'] = t('This page is currently empty.');
    return $error;
  }

  $copyright = asciidoc_display_find_copyright($page_info['copyright_file']);
  if ($copyright) {
    $copyright = '<div class="copyright">' . $copyright . '</div>';
  }

  return $html_info['body'] . $copyright;
}

/**
 * Page callback: Puts up the AsciiDoc source editor.
 */
function asciidoc_display_direct_edit_source() {
  global $base_url;

  // If a problem occurs, we'll want to return a message.
  $error = array(
    '#type' => 'markup',
    '#markup' => '',
  );

  // Figure out what file we want to edit.
  $arg = arg();
  $page_info = _asciidoc_display_direct_find_page($arg);
  if (!$page_info['book'] || !$page_info['edit_file']) {
    $error['#markup'] = t('Could not load page');
    return $error;
  }

  $file = $page_info['edit_file'];
  $config = $page_info['book'];
  if (!$config['source_path']) {
    $error['#markup'] = t('Could not load page');
    return $error;
  }

  // Display it in the editor.
  $do_patches = asciidoc_display_direct_patches_allowed($config, FALSE);
  return drupal_get_form('asciidoc_display_edit_form', $file, $config['source_path'], $config['translated'], $page_info['language'], $config['source_language'], 'edit/' . $config['path'], $do_patches, $config['line_endings'], $base_url . '/' .  $config['path'] . '/' . $page_info['language']);
}

/**
 * Generates the navigation block, if we are on an AsciiDoc display page.
 */
function asciidoc_display_direct_navigation_block() {
  $arg = arg();
  $page_info = _asciidoc_display_direct_find_page($arg);

  if ($page_info['redirect'] || !$page_info['book'] || $page_info['edit_file']) {
    // Display nothing if this is not an AsciiDoc page.
    return array();
  }

  $path = drupal_realpath($page_info['index_file']);
  $html = @file_get_contents($path);
  if (!$html) {
    // Display nothing if we did not locate the index file.
    return array();
  }

  // Make a link to the front page.
  $config = $page_info['book'];
  $home_link = l(t('Cover page'), $config['path'], array('attributes' => array('class' => 'asciidoc_cover_link')));

  // Display the table of contents.
  $html_info = asciidoc_display_parse_bare_html($html, array('keep_toc' => $page_info['is_index'], 'toc_active' => $page_info['page_filename']));

  return array(
    'subject' => $html_info['book_title'],
    // @todo This should be made more flexible and themeable.
    'content' => array(
      '#markup' => $home_link . ' <div class="asciidoc-display-toc"> ' . $html_info['toc'] . '</div>',
      '#attached' => array(
        'library' => array(array('asciidoc_display_direct', 'asciidoc_display_direct.nav')),
      ),
    ),
  );
}

/**
 * Generates the language block, if we are on an AsciiDoc display page.
 */
function asciidoc_display_direct_language_block() {
  $arg = arg();
  $page_info = _asciidoc_display_direct_find_page($arg);

  if ($page_info['redirect'] || !$page_info['book'] || $page_info['edit_file']) {
    // Display nothing if this is not an AsciiDoc page.
    return array();
  }

  $config = $page_info['book'];
  if (!$config['translated']) {
    // Display nothing if this book is not translated.
    return array();
  }

  $languages = asciidoc_display_list_languages($config['dir'], 'links', $config['path']);
  if (!count($languages)) {
    // Display nothing if there are no languages.
    return array();
  }

  $build = array();
  $build['title'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . $page_info['book']['title'] . '</p>',
  );

  $build['links'] = array(
    '#theme' => 'links__asciidoc_display_direct_languages',
    '#links' => $languages,
    '#attributes' => array('class' => array('asciidoc_display_direct_languages')),
  );

  return array(
    'subject' => t('Languages'),
    'content' => $build,
  );
}

/**
 * Generates the edit block, if we are on an AsciiDoc display page.
 */
function asciidoc_display_direct_edit_block() {
  $arg = arg();
  $page_info = _asciidoc_display_direct_find_page($arg);

  if ($page_info['redirect'] || !$page_info['book']) {
    // Display nothing if this is not an AsciiDoc page.
    return array();
  }
  $config = $page_info['book'];

  // Build some links for this page.
  $items = array();

  // Add a link to the project page, if it is defined.
  if ($config['project_name'] && $config['project_url']) {
    $items[] = t('This page is generated from AsciiDoc source in the !link project',
      array('!link' => l($config['project_name'], $config['project_url'])));
  }

  // Add a file issue link, if it is defined.
  if ($config['issue']) {
    $issue_query = array(
      'component' => 'Documentation',
      // Bug reports are category = 1.
      'categories' => 1,
      // Do not translate the issue link.
      'body' => 'RE: ' . check_url(request_uri()),
    );
    $items[] = l(t('File an issue'), $config['issue'], array('query' => $issue_query));
  }

  // Add edit links for all of the files being used to generate this page, if
  // we are not already on an Edit page and the user can edit.
  if (!$page_info['edit_file'] && user_access('edit asciidoc source')) {
    $path = drupal_realpath($page_info['page_file']);
    $html = @file_get_contents($path);
    if ($html) {
      // Find all files marked as being used in the page.
      module_load_include('inc', 'asciidoc_display_direct', 'asciidoc_display_direct.pages');
      $html_info = asciidoc_display_parse_bare_html($html, array('keep_toc' => $page_info['is_index'], 'highlight_php' => TRUE));

      foreach ($html_info['files'] as $file) {
        $language_part = ($page_info['language'] ? $page_info['language'] . '/' : '');
        $items[$file] = l(t('Edit source file @file', array('@file' => $file)), 'edit/' . $config['path'] . '/' . $language_part . $file);
      }
    }
  }

  // Add a link to the AsciiDoc user manual if we are editing, and a link back
  // to the book.
  if ($page_info['edit_file']) {
    $items[] = l(t('AsciiDoc User Manual'), 'http://asciidoc.org/userguide.html');
    $items[] = l(t('Return to book'), $config['path']);
  }

  // Put it all together and return it, if we have any links.
  if (!count($items)) {
    return array();
  }

  $build = array();
  $build['edits'] = array(
    '#theme' => 'item_list',
    '#items' => $items,
  );

  return array(
    'subject' => t('Source information'),
    'content' => $build,
  );
}

/**
 * Parses an AsciiDoc URL path.
 *
 * The path passed into this function is assumed to look something like this,
 * where 'foo' is the book's base path, 'en' is a language code, and the
 * somefile and somedir are arbitrary file and directory names:
 * - edit/foo/en/somefile.txt
 * - foo/en/somefile.html
 * - foo/en/somefile.png
 * - foo/en/somedir/someimage.png
 * - And the same as all the above, but without the /en part (for untranslated
 *   books).
 *
 * @param string[] $args
 *   Page arguments array from the path.
 *
 * @return array
 *   Array of information about possible ways the path could be parsed. Each
 *   is an array, with the following keys:
 *   - path: The book's base path.
 *   - redirect: TRUE if this path is missing everything but the book path,
 *     in which case you would want to redirect to the first page of the book.
 *     FALSE for other path information arrays.
 *   - translated: TRUE if there was a language code, FALSE if not.
 *   - language: Language code found in the path.
 *   - page_filename: The name of the HTML file that was found in the path;
 *     FALSE if it is an image, and 'index.html' if editing.
 *   - edit_file: The name of the file to edit, or FALSE if not editing.
 *   - image_file: The name of the image file, or FALSE if there is none.
 *   - image_dir: The directory of the image file; missing if not an image.
 *
 * @see asciidoc_display_validate()
 */
function _asciidoc_display_direct_parse_path($args) {
  $found = &drupal_static(__FUNCTION__);

  // See if we've already calculated this for this request.
  $key = implode('/', $args);
  if (isset($found[$key])) {
    return $found[$key];
  }

  // See if it's in the cache.
  $cached = cache_get('asciidoc_display_direct_parse_path');
  if ($cached) {
    $cached = $cached->data;
  }
  else {
    $cached = array();
  }
  if (isset($cached[$key])) {
    $found[$key] = $cached[$key];
    return $cached[$key];
  }

  // It's not in our static or the cache, so we need to calculate it.

  // See if we might be on an edit page.
  $could_be_edit = (count($args) > 0 && $args[0] == 'edit');

  // Pop off the (supposed) file name. If the last bit doesn't end in .html,
  // we will need to redirect (unless it is an edit or image page).
  $filename = array_pop($args);
  $extension = pathinfo($filename, PATHINFO_EXTENSION);
  $on_html = ($extension == 'html');
  $is_img = in_array(strtolower($extension), array('jpg', 'jpeg', 'png', 'gif'));

  // Figure out possible paths, with edit/ prefix, languages, etc.
  $possible_paths = array();

  if ($on_html) {
    $path = implode('/', $args);
    $possible_paths['plain'] = array(
      'path' => $path,
      'translated' => FALSE,
      'edit_file' => FALSE,
      'page_filename' => $filename,
      'image_file' => FALSE,
      'language' => '',
      'redirect' => FALSE,
    );

    if (count($args) > 1) {
      $newargs = $args;
      $language = array_pop($newargs);
      $path = implode('/', $newargs);
      $possible_paths['plain_language'] = array(
        'path' => $path,
        'translated' => TRUE,
        'edit_file' => FALSE,
        'image_file' => FALSE,
        'language' => $language,
        'page_filename' => $filename,
        'redirect' => FALSE,
      );
    }
  }
  if ($is_img) {
    $path = implode('/', $args);
    $possible_paths['image'] = array(
      'path' => $path,
      'translated' => FALSE,
      'edit_file' => FALSE,
      'page_filename' => FALSE,
      'image_file' => $filename,
      'image_dir' => '',
      'language' => '',
      'redirect' => FALSE,
    );

    if (count($args) > 1) {
      // Could have a language.
      $langargs = $args;
      $language = array_pop($langargs);
      $path = implode('/', $langargs);
      $possible_paths['image_language'] = array(
        'path' => $path,
        'edit_file' => FALSE,
        'page_filename' => FALSE,
        'image_file' => $filename,
        'image_dir' => '',
        'translated' => TRUE,
        'language' => $language,
        'redirect' => FALSE,
      );

      // Also support an image subdirectory.
      $subargs = $args;
      $subdir = array_pop($subargs);
      $path = implode('/', $subargs);
      $possible_paths['image_subdir'] = array(
        'path' => $path,
        'translated' => FALSE,
        'edit_file' => FALSE,
        'page_filename' => FALSE,
        'image_dir' => $subdir,
        'image_file' => $filename,
        'language' => '',
        'redirect' => FALSE,
      );

      // And finally, and image subdirectory plus language.
      if (count($subargs) > 1) {
        $langargs = $subargs;
        $language = array_pop($langargs);
        $path = implode('/', $langargs);
        $possible_paths['image_subdir_language'] = array(
          'path' => $path,
          'edit_file' => FALSE,
          'page_filename' => FALSE,
          'image_dir' => $subdir,
          'image_file' => $filename,
          'translated' => TRUE,
          'language' => $language,
          'redirect' => FALSE,
        );
      }
    }
  }
  elseif ($could_be_edit) {
    $newargs = $args;
    array_shift($newargs);
    $path = implode('/', $newargs);
    $possible_paths['edit'] = array(
      'path' => $path,
      'translated' => FALSE,
      'edit_file' => $filename,
      'image_file' => FALSE,
      'page_filename' => 'index.html',
      'language' => '',
      'redirect' => FALSE,
    );

    if (count($newargs) > 1) {
      $language = array_pop($newargs);
      $path = implode('/', $newargs);
      $possible_paths['edit_language'] = array(
        'path' => $path,
        'translated' => TRUE,
        'edit_file' => $filename,
        'language' => $language,
        'image_file' => FALSE,
        'page_filename' => 'index.html',
        'redirect' => FALSE,
      );
    }
  }
  else {
    // Make one more path with the filename assumed missing, as a redirect.
    $args[] = $filename;
    $path = implode('/', $args);
    $possible_paths['redirect'] = array(
      'path' => $path,
      'redirect' => TRUE,
    );
  }

  // Set in static, and in cache, and return.
  $found[$key] = $possible_paths;
  $cached[$key] = $possible_paths;
  cache_set('asciidoc_display_direct_parse_path', $cached);

  return $possible_paths;
}

/**
 * Figures out which page of which AsciiDoc book we are on.
 *
 * The path looks something like this, where "(path)" is the path from the
 * individual book config, which might have multiple components:
 * @code
 * [edit/](path)/[langcode/][/filename]
 * @endcode
 * We need to match (path) and the edit/langcode parts with an existing config
 * and figure out which filename we are on (or redirect to index.html if none
 * is given).
 *
 * @param string[] $args
 *   Page arguments array, for a page display or source edit page.
 *
 * @return string[]
 *   Array of information, containing:
 *   - redirect: Path this page should be redirected to, if set. If set,
 *     nothing else will be set. The path comes from the configured path for
 *     the book that is matched, plus the configured default language.
 *   - book: Configuration for the book we are in, or FALSE if we are not on a
 *     valid book page. If FALSE, the rest of the information will be
 *     omitted. This information comes straight from the configuration form and
 *     has not been further sanitized.
 *   - language: The language code for the page we are on, or '' if the book is
 *     not translated. This is checked to make sure it is a known language.
 *   - edit_file: File name we are editing, if on an edit page, or FALSE if we
 *     are not on an edit page. If set, the rest of the info will be as if we
 *     were on the index page. This is validated to make sure the file name is
 *     legal.
 *   - image_file: Full path to image file, if the request is for an image.
 *   - page_filename: File name of the HTML file of the current page. This is
 *     validated to make sure the file name is legal.
 *   - page_file: Full path to the HTML file of the current page, made up of
 *     the page_filename and language information, along with the configured
 *     file path for the book.
 *   - index_file: Full path to the HTML file containing the book index, made up
 *     of the language information along with the configured file path for the
 *     book.
 *   - copyright_file: Full path to the HTML file, which might or might not
 *     exist, containing the copyright notice. This is made up from the
 *     configured information for the book, plus language information.
 *   - is_index: TRUE if this is the index page.
 *
 * @see asciidoc_display_validate()
 */
function _asciidoc_display_direct_find_page($args) {
  $found = &drupal_static(__FUNCTION__);

  // See if we've already calculated this for this request.
  $key = implode('/', $args);
  if (isset($found[$key])) {
    return $found[$key];
  }

  // See if it's in the cache.
  $cached = cache_get('asciidoc_display_direct_find_page');
  if ($cached) {
    $cached = $cached->data;
  }
  else {
    $cached = array();
  }
  if (isset($cached[$key])) {
    $found[$key] = $cached[$key];
    return $cached[$key];
  }

  // It's not in our static or the cache, so we need to calculate it.

  // Try to find a matching configuration for one of the possible paths.
  $possible_paths = _asciidoc_display_direct_parse_path($args);
  $configs = asciidoc_display_direct_load_all_configs();
  foreach ($configs as $config) {
    foreach ($possible_paths as $pathinfo) {
      if ($config['path'] == $pathinfo['path'] && $pathinfo['redirect']) {
        $language = $config['translated'] ? '/' . $config['primary_language'] : '';
        $info = array(
          'redirect' => $config['path'] . $language . '/index.html',
        );
        $found[$key] = $info;
        $cached[$key] = $info;
        cache_set('asciidoc_display_direct_find_page', $cached);
        return $info;
      }
      elseif ($config['path'] == $pathinfo['path'] && $config['translated'] == $pathinfo['translated']) {
        $filename = $pathinfo['page_filename'];
        $language = $config['translated'] ? '/' . $pathinfo['language'] : '';
        if (asciidoc_display_validate($pathinfo['language'], $filename)) {
          $info = $pathinfo + array(
            'book' => $config,
            'page_file' => $config['dir'] . $language . '/' . $filename,
            'index_file' => $config['dir'] . $language . '/index.html',
            'copyright_file' => $config['dir'] . $language . '/' . $config['copyright_file'],
            'is_index' => ($filename == 'index.html'),
            'redirect' => FALSE,
          );
          if ($info['image_file']) {
            if (asciidoc_display_validate('', $info['image_dir'])) {
              $subdir = $info['image_dir'] ? '/' . $info['image_dir'] : '';
              $info['image_file'] = $config['dir'] . $language . $subdir . '/' . $info['image_file'];
            }
          }
          $found[$key] = $info;
          $cached[$key] = $info;
          cache_set('asciidoc_display_direct_find_page', $cached);
          return $info;
        }
      }
    }
  }

  // If we got here, we never found a match.
  $info = array(
    'redirect' => FALSE,
    'book' => 0,
  );
  $found[$key] = $info;
  $cached[$key] = $info;
  cache_set('asciidoc_display_direct_find_page', $cached);

  return $info;
}
