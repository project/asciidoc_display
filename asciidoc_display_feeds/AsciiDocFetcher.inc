<?php

/**
 * @file
 * Contains the AsciiDocFetcher and AsciiDocFetcherResult classes.
 */

/**
 * Feed fetcher result class for AsciiDocFetcher.
 */
class AsciiDocFetcherResult extends FeedsFileFetcherResult {

  /**
   * Constructs an AsciiDocFetcherResult object.
   *
   * @param string $file_path
   *   File path being parsed.
   * @param string $copyright
   *   Copyright notice to be appended to page bodies.
   * @param string $langcode
   *   Language of file.
   * @param array $parents
   *   Array giving parent file and weight of each file in the book.
   * @param string $source_langcode
   *   Source language for the book.
   */
  public function __construct($file_path, $copyright, $langcode = '', $parents = array(), $source_langcode = '') {
    parent::__construct($file_path);
    $this->copyright = $copyright;
    $this->langcode = $langcode ? $langcode : language_default('language');
    $this->parents = $parents;
    $this->source_langcode = $source_langcode ? $source_langcode : language_default('language');
  }

  /**
   * Returns the copyright notice to be appended to page bodies.
   */
  public function getCopyright() {
    return $this->copyright;
  }

  /**
   * Returns the language code of the file, or the site default language.
   */
  public function getLanguageCode() {
    return $this->langcode;
  }

  /**
   * Returns the source language of the book, or the site default language.
   */
  public function getSourceLanguageCode() {
    return $this->source_langcode;
  }

  /**
   * Returns the parents array.
   *
   * @return array
   *   Array keyed by output file name. The values are each arrays, with
   *   elements:
   *   - file: Name of the parent file name in the table of contents.
   *   - weight: Weight of this entry within the table of contents.
   */
  public function getParents() {
    return $this->parents;
  }

}

/**
 * Fetcher class for AsciiDoc output files.
 *
 * This class works like the base FeedsFileFetcher class, with the following
 * changes:
 * - It requires the input to be a directory, not a single file.
 * - Within this directory, it processes all language sub-directories.
 * - For each language directory, the copyright and topic hierarchy are read
 *   from specific files to start the process. The companion AsciiDocParser
 *   class uses this information when parsing the files.
 */
class AsciiDocFetcher extends FeedsFileFetcher {

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $state = $source->state(FEEDS_FETCH);
    module_load_include('inc', 'asciidoc_display', 'asciidoc_display');

    // Initialize the language list, if not already done.
    if (!isset($state->languages)) {
      if ($this->config['translated']) {
        $languages = array_keys(asciidoc_display_list_languages($source_config['source']));
      }
      else {
        $languages = array(language_default('language'));
      }
      $state->languages = array_combine($languages, $languages);
      $state->files = array();
      unset($state->total);
    }

    // Initialize the files, copyright, and parents list for the next language,
    // if there is nothing in the file list.
    if (!count($state->files)) {
      // If the configured source language is in the list, make sure it is
      // fetched first.
      $source_language = isset($this->config['source_language']) ? $this->config['source_language'] : language_default('language');
      if (isset($state->languages[$source_language])) {
        $state->langcode = $source_language;
        unset($state->languages[$source_language]);
      }
      else {
        $state->langcode = array_shift($state->languages);
      }

      $filedir = $this->config['translated'] ? $source_config['source'] . '/' . $state->langcode : $source_config['source'];
      $files_in_dir = $this->listFiles($filedir);
      if (!count($files_in_dir)) {
        throw new Exception(t('Source is not a file or it is an empty directory: %source', array('%source' => $filedir)));
      }

      // Find the copyright file and read the copyright notice from it.
      $copyright_file = $filedir . '/' . $this->config['copyright_file'];
      $state->copyright = asciidoc_display_find_copyright($copyright_file);

      // Find the index file and read in the parents list.
      $index_file = $filedir . '/index.html';
      $state->parents = asciidoc_display_list_parents($index_file);

      // Order the files by the parents list, so that menu and book hierarchies
      // will have the parent defined before the child.
      $state->files = $this->orderFiles($files_in_dir, $state->parents);

      // This may be an approximate count, if not all languages have the
      // same number of files.
      $state->remaining = (count($state->languages) + 1) * count($state->files);
      if (!isset($state->total)) {
        $state->total = $state->remaining;
      }
    }

    // Process one file.
    $file = array_shift($state->files);
    $state->remaining--;
    $state->progress($state->total, $state->total - $state->remaining);
    return new AsciiDocFetcherResult($file, $state->copyright, $state->langcode, $state->parents, $this->config['source_language']);
  }

  /**
   * {@inheritdoc}
   */
  public function configDefaults() {
    return array(
      'allowed_extensions' => 'html',
      'direct' => TRUE,
      'translated' => FALSE,
      'source_language' => 'en',
      'copyright_file' => '',
      'source_directory' => '',
      'source_field' => '',
      'source_message' => '',
    ) + parent::configDefaults();
  }

  /**
   * {@inheritdoc}
   */
  public function configForm(&$form_state) {
    // The base FeedsFileFetcher has a number of configuration options, which
    // we do not want. So just embed fixed values into the form.
    // Also note that since an average AsciiDoc import will have a bunch of
    // configuration on the Mappers and Parser, it doesn't make sense to use
    // Source configuration for the configuration of the Fetcher, so it is all
    // here in the config form, except for specifying the directory (which the
    // parent class expects to be in Source configuration).
    $form = array();
    $from_parent = array_keys(parent::configDefaults());
    $defaults = $this->configDefaults();
    foreach ($from_parent as $key) {
      $form[$key] = array(
        '#type' => 'value',
        '#value' => $defaults[$key],
      );
    }

    $form['translated'] = array(
      '#type' => 'checkbox',
      '#title' => t('Translated'),
      '#description' => t('Whether this book is translated or not. If it is, the AsciiDoc source and output is assumed to be in subdirectories under the main output directory, with the two-letter language code as the directory name'),
      '#default_value' => $this->config['translated'],
    );

    $form['source_language'] = array(
      '#type' => 'textfield',
      '#title' => t('Source language'),
      '#description' => t('For translated books, the source language (two-letter code).'),
      '#default_value' => $this->config['source_language'],
    );

    $form['copyright_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Copyright file'),
      '#description' => t('Name of the file, within the output directory, that contains the copyright notice to put at the bottom of each page. Leave blank to omit.'),
      '#default_value' => $this->config['copyright_file'],
    );

    $form['source_directory'] = array(
      '#type' => 'textfield',
      '#title' => t('Source directory'),
      '#description' => t('To enable source file editing, enter the directory where source files can be found here.'),
      '#default_value' => $this->config['source_directory'],
    );

    $form['source_field'] = array(
      '#type' => 'textfield',
      '#title' => t('Source file field'),
      '#description' => t('To enable source file editing, you need to save the source file information, using a mapper, in a field on your imported content types. Enter the machine name of the field you saved it in here.'),
      '#default_value' => $this->config['source_field'],
    );

    $form['source_message'] = array(
      '#type' => 'textarea',
      '#title' => t('Source editing message'),
      '#description' => t('Message to appear in the source editing block, if source editing is set up. Can be blank. Can contain HTML.'),
      '#default_value' => $this->config['source_message'],
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function sourceForm($source_config) {
    // See note in the AsciiDocFetcher::configForm() code about what is here
    // vs. there.

    $source_config += array('source' => '');

    // The parent expects this.
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => 0,
    );

    // Modified version of the 'source' config to specify that it is always
    // a directory, specified as a full path.
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('Directory'),
      '#description' => t('Specify a path to a directory.'),
      '#default_value' => $source_config['source'],
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function sourceFormValidate(&$values) {
    $dir = drupal_realpath($values['source']);
    if (!is_dir($dir)) {
      form_set_error('feeds][AsciiDocFetcher][source', t('The specified directory does not exist.'));
    }
  }

  /**
   * Reorders the files in $files based on the order of the $parents array.
   */
  protected function orderFiles($files, $parents) {
    $files_in_parents = array_keys($parents);
    $basenames = array_map('basename', $files);
    $files_by_basename = array_combine($basenames, $files);

    $ordered = array();
    foreach (array_intersect($files_in_parents, $basenames) as $basename) {
      $ordered[] = $files_by_basename[$basename];
    }
    $ordered += array_diff($files, $ordered);

    return $ordered;
  }

}
