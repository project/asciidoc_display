<?php

/**
 * @file
 * Page-generating and block functions for AsciiDoc feeds.
 */


/**
 * Page callback: Puts up the AsciiDoc source editor.
 *
 * Note that the $node input here should have been passed through
 * asciidoc_display_feeds_menu_node_load(), so it will have the
 * $node->asciidoc_display_feeds_info component set.
 */
function asciidoc_display_feeds_edit_source($node) {

  module_load_include('inc', 'asciidoc_display', 'asciidoc_display');

  $feed_info = $node->asciidoc_display_feeds_info;

  return drupal_get_form('asciidoc_display_edit_form', $feed_info['source_file'], $feed_info['source_directory'], $feed_info['translated'], $feed_info['language'], $feed_info['source_language'], '', FALSE, 'unix', $feed_info['image_dir']);
}

/**
 * Generates the edit block, if we are on an imported AsciiDoc node page.
 */
function asciidoc_display_feeds_edit_block() {
  $arg = arg();
  if (count($arg) < 2 || $arg[0] != 'node') {
    // This is not a node page at all, so display nothing.
    return array();
  }
  $nid = (int) $arg[1];
  if (!$nid) {
    return array();
  }
  $node = node_load($nid);
  $feed_info = _asciidoc_display_feeds_get_feeds_info($node);
  if (!$feed_info) {
    // This is not a node that was imported using an AsciiDoc feed.
    return array();
  }

  $items = array();
  if ($feed_info['block_message']) {
    $items['message'] = array(
      '#type' => 'markup',
      '#markup' => '<div>' . filter_xss_admin($feed_info['block_message']) . '</div>',
    );
  }

  if ($feed_info['source_file']) {
    $items['source_file'] = array(
      '#type' => 'markup',
      '#markup' => '<p>' . t('Source file: @file', array('@file' => $feed_info['source_file'])) . '</p>',
    );

    // Add edit link for the source file being used to generate this page, if
    // user can edit.
    if (user_access('edit asciidoc source')) {
      $items['edit'] = array(
        '#type' => 'markup',
        '#markup' => '<p>' . l(t('Edit AsciiDoc source file'), 'node/' . $nid . '/asciidoc_edit') . '</p>',
      );
    }
  }

  // Put it all together and return it, if we have any links.
  if (!count($items)) {
    return array();
  }

  return array(
    'subject' => t('Source information'),
    'content' => $items,
  );
}
