<?php

/**
 * @file
 * Contains the AsciiDocParser class.
 */

/**
 * Parses AsciiDoc output for the Feeds module.
 *
 * If used with the AsciiDocFetcher class:
 * - The copyright notice is added to the end of the Body field if configured.
 * - The language is set to the language of the directory being processed.
 * - The parent fields are set (see the AsciiDocParser::getMappingSources()
 *   method) based on the hierarchy parsed from the index file's table of
 *   contents.
 */
class AsciiDocParser extends FeedsParser {

  /**
   * {@inheritdoc}
   */
  function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {

    // Get some information about the file.
    $state = $source->state(FEEDS_PARSE);
    $contents = $fetcher_result->getRaw();
    $copyright = '';
    $type = 'topic';
    $langcode = language_default('language');
    $parents = array();
    $source_langcode = $langcode;
    if ($fetcher_result instanceof AsciiDocFetcherResult) {
      $copyright = $fetcher_result->getCopyright();
      $langcode = $fetcher_result->getLanguageCode();
      $parents = $fetcher_result->getParents();
      $source_langcode = $fetcher_result->getSourceLanguageCode();
    }

    $link_prefix = _asciidoc_display_feeds_replace_language($this->config['internal_link_prefix'], $langcode, TRUE);
    $image_prefix = _asciidoc_display_feeds_replace_language($this->config['internal_image_prefix'], $langcode);

    // Parse the file and calculate some values to return.
    module_load_include('inc', 'asciidoc_display', 'asciidoc_display');
    $options =  array(
      'keep_toc' => $this->config['show_toc'],
      'link_prefix' => $link_prefix,
      'image_prefix' => $image_prefix,
      'remove_h1_titles' => $this->config['remove_h1_titles'],
      'remove_h2_titles' => $this->config['remove_h2_titles'],
      'remove_source_line' => TRUE,
    );
    $parsed = asciidoc_display_parse_bare_html($contents, $options);

    $title = $this->replaceNonBreakingSpaces($parsed['section_title']);
    // The source file is the last one in the list, in case there are several.
    $source_file = array_pop($parsed['files']);
    $filename = basename($fetcher_result->getFilePath());
    $parent = isset($parents[$filename]) ? $parents[$filename] : array('file' => '', 'weight' => 0);
    $guid = implode('--', array($this->config['guid_prefix'], $langcode, $filename));
    $parent_guid = implode('--', array($this->config['guid_prefix'], $langcode, $parent['file']));
    $source_guid = implode('--', array($this->config['guid_prefix'], $source_langcode, $filename));
    $dir = dirname($fetcher_result->getFilePath());
    $image_paths = array();
    foreach ($parsed['images'] as $image) {
      $image_paths[] = drupal_realpath($dir . '/' . $image);
    }

    // Return the result.
    $item = array(
      'title' => $title,
      'body' => $parsed['body'] . ($this->config['add_copyright'] ? $copyright : ''),
      'summary' => $parsed['summary'],
      'source_file' => $source_file,
      'output_file' => $filename,
      'parent_file' => $parent['file'],
      'parent_weight' => $parent['weight'],
      'guid' => $guid,
      'parent_guid' => $parent_guid,
      'source_guid' => $source_guid,
      'images' => $image_paths,
      'type' => $parsed['type'],
      'langcode' => $langcode,
    );

    $state->progress(1,1);
    return new FeedsParserResult(array($item));
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingSources() {
    return array(
      'title' => array(
        'name' => t('Title'),
        'description' => t('Title of the topic'),
      ),
      'body' => array(
        'name' => t('Body'),
        'description' => t('HTML body of the topic'),
      ),
      'summary' => array(
        'name' => t('Summary'),
        'description' => t('Summary of the topic, if defined in the source'),
      ),
      'guid' => array(
        'name' => t('GUID'),
        'description' => t('Unique ID, including the book GUID prefix, language, and file name'),
      ),
      'source_file' => array(
        'name' => t('Source file'),
        'description' => t('File name of the AsciiDoc source for the page'),
      ),
      'output_file' => array(
        'name' => t('Output file'),
        'description' => t('File name of the AsciiDoc output for the page. Use this as the end of the URL alias for the created content item, so that internal links between topics will work.'),
      ),
      'parent_file' => array(
        'name' => t('Parent file'),
        'description' => t('Output file name of the parent of the page in the table of contents hierarchy, if it can be determined.'),
      ),
      'parent_weight' => array(
        'name' => t('Parent weight'),
        'description' => t('Weight (order) in the table of contents, if it can be determined'),
      ),
      'parent_guid' => array(
        'name' => t('Parent GUID'),
        'description' => t('Unique ID of the parent of the page in the table of contents hierarchy, if it can be determined.'),
      ),
      'source_guid' => array(
        'name' => t('Translation source GUID'),
        'description' => t('Unique ID of the translation source of the page'),
      ),
      'images' => array(
        'name' => t('Images'),
        'description' => t('Array of paths to images referenced in the page'),
      ),
      'type' => array(
        'name' => t('Type'),
        'description' => t('Page type: Either topic, chapter, or book'),
      ),
      'langcode' => array(
        'name' => t('Language'),
        'description' => t('Language code for the page, or the site default langauge if unknown'),
      ),
    ) + parent::getMappingSources();
  }

  /**
   * {@inheritdoc}
   */
  public function providesSourceTitle() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function configDefaults() {
    return array(
      'internal_link_prefix' => '',
      'internal_image_prefix' => '',
      'show_toc' => FALSE,
      'add_copyright' => TRUE,
      'remove_h1_titles' => FALSE,
      'remove_h2_titles' => FALSE,
      'guid_prefix' => '',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function configForm(&$form_state) {
    $form = array();

    $form['guid_prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('GUID prefix'),
      '#description' => t('The GUID field is composed of this prefix, the language code, and the output file name, to make it unique across all imported items in all books'),
      '#default_value' => $this->config['guid_prefix'],
    );

    $form['internal_link_prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Internal link prefix'),
      '#description' => t('Add this prefix to the URL/path of all internal links to other topics, when importing the HTML (the link URLs will otherwise be the bare output file names). You can put [LANG] into your prefix to be replaced by the language code. Do not include the language URL prefix or domain added for non-default languages by the language system.'),
      '#default_value' => $this->config['internal_link_prefix'],
    );

    $form['internal_image_prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Internal image prefix'),
      '#description' => t('Add this prefix to the URL/path of all images whose source URL is relative to the page, when importing the HTML. You can put [LANG] into your prefix to be replaced by the language code.'),
      '#default_value' => $this->config['internal_image_prefix'],
    );

    $form['show_toc'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include table of contents'),
      '#description' => t('If checked, the page body will include the table of contents, for book and chapter pages. Otherwise, the table of contents will be stripped from all pages, and book/chapter pages may be blank.'),
      '#default_value' => $this->config['show_toc'],
    );

    $form['add_copyright'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include copyright'),
      '#description' => t('If checked, the page body will include a copyright notice at the bottom'),
      '#default_value' => $this->config['add_copyright'],
    );

    $form['remove_h1_titles'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove chapter (h1) titles'),
      '#description' => t('If checked, all H1 tags in the page body with class "title" will be removed.'),
      '#default_value' => $this->config['remove_h1_titles'],
    );

    $form['remove_h2_titles'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove section (h2) titles'),
      '#description' => t('If checked, all H2 tags in the page body with class "title" will be removed.'),
      '#default_value' => $this->config['remove_h2_titles'],
    );

    return $form;
  }

  /**
   * Replaces non-breaking spaces with regular spaces.
   *
   * @param string $text
   *   Text to replace spaces in.
   *
   * @return string
   *   Text with non-breaking space characters replaced with regular spaces.
   */
  protected function replaceNonBreakingSpaces($text) {
    $text = htmlentities($text);
    $text = str_replace('&nbsp;', ' ', $text);
    $text = html_entity_decode($text);
    return $text;
  }

}
