<?php

/**
 * @file
 * AsciiDoc Display Feeds module hooks.
 */

/**
 * Implements hook_help().
 */
function asciidoc_display_feeds_help($path, $arg) {
  switch ($path) {
    case 'admin/help#asciidoc_display_feeds':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The AsciiDoc Display Feeds module integrates with the Feeds module, to allow you to import output from the AsciiDoc documentation formatting system into Drupal content items, if you use the scripts provided with the module to generate the output. A set of AsciiDoc documentation is known as a book, and this module can import one or more books. See the README.txt file in the project folder for installation and configuration information.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Importing books') . '</dt>';
      $output .= '<dd>' . t('After generating specially-formatted HTML output from the AsciiDoc tool chain, you can use the Feeds module to import it. Use the AsciiDoc Fetcher and AsciiDoc Parser components for best results.') . '</dd>';
        $output .= '<dt>' . t('Editing source files') . '</dt>';
        $output .= '<dd>' . t('Users with appropriate permission can use an AsciiDoc editor on the site to edit the source files for the book. Editing does not directly change any files, but users can download their changes locally. To configure editing, fill in the source directory and related fields in your AsciiDoc Fetcher configuration on the feed.') . '</dd>';
        $output .= '<dt>' . t('Configuring blocks') . '</dt>';
        $output .= '<dd>' . t('This module provides a block that provides a link to edit pages. This block can be displayed on the page for an imported content item.') . '</dd>';

      $output .= '</dl>';

      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function asciidoc_display_feeds_menu() {
  $items['node/%asciidoc_display_feeds_menu_node/asciidoc_edit'] = array(
    'title' => 'Edit AsciiDoc source',
    'page callback' => 'asciidoc_display_feeds_edit_source',
    'page arguments' => array(1),
    'access arguments' => array('edit asciidoc source'),
    'file' => 'asciidoc_display_feeds.pages.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Auto-loader for asciidoc_display_feeds_menu() for node/$nid/asciidoc_edit.
 */
function asciidoc_display_feeds_menu_node_load($nid) {
  if (!$nid) {
    return FALSE;
  }

  $node = node_load($nid);
  $info = _asciidoc_display_feeds_get_feeds_info($node);
  if (!$info || !$info['source_file']) {
    return FALSE;
  }
  $node->asciidoc_display_feeds_info = $info;

  return $node;
}

/**
 * Implements hook_block_info().
 */
function asciidoc_display_feeds_block_info() {
  $blocks['page_edit'] = array(
    'info' => t('AsciiDoc Display Feeds edit source'),
    'cache' => DRUPAL_CACHE_PER_ROLE | DRUPAL_CACHE_PER_PAGE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function asciidoc_display_feeds_block_view($delta = '') {
  switch ($delta) {
    case 'page_edit':
      module_load_include('inc', 'asciidoc_display_feeds', 'asciidoc_display_feeds.pages');
      return asciidoc_display_feeds_edit_block();
  }
}

/**
 * Implements hook_feeds_plugins().
 */
function asciidoc_display_feeds_feeds_plugins() {
  $info = array();

  $info['AsciiDocFetcher'] = array(
    'name' => t('AsciiDoc Fetcher'),
    'description' => t('Gathers AsciiDoc output from a directory for parsing'),
    'handler' => array(
      'parent' => 'FeedsFileFetcher',
      'class' => 'AsciiDocFetcher',
      'file' => 'AsciiDocFetcher.inc',
      'path' => drupal_get_path('module', 'asciidoc_display_feeds'),
    ),
  );

  $info['AsciiDocParser'] = array(
    'name' => t('AsciiDoc Parser'),
    'description' => t('Parses AsciiDoc output fetched by the AsciiDoc Fetcher'),
    'handler' => array(
      'parent' => 'FeedsParser',
      'class' => 'AsciiDocParser',
      'file' => 'AsciiDocParser.inc',
      'path' => drupal_get_path('module', 'asciidoc_display_feeds'),
    ),
  );

  return $info;
}

/**
 * Implements hook_feeds_processor_targets().
 */
function asciidoc_display_feeds_feeds_processor_targets($entity_type, $bundle) {
  $targets['asciidoc_files'] = array(
    'name' => t('AsciiDoc file upload'),
    'description' => t('Upload files to a directory in the public files area'),
    'callback'  => '_asciidoc_display_feeds_files_set_target',
    'form_callbacks' => array('_asciidoc_display_feeds_files_config_form'),
    'summary_callbacks' => array('_asciidoc_display_feeds_files_summary'),
  );

  // All of the rest of the targets in this function are specific to nodes,
  // so return if the entity is not a node.
  if ($entity_type != 'node') {
    return $targets;
  }

  // Menu item code modified from https://www.drupal.org/sandbox/attisan/2144379
  if (module_exists('menu')) {
    $targets['asciidoc_menu_title'] = array(
      'name' => t('Menu link text'),
      'description' => t('Link text for the menu link.'),
      'callback'  => '_asciidoc_display_feeds_menu_title_set_target',
      'form_callbacks' => array('_asciidoc_display_feeds_menu_title_config_form'),
      'summary_callbacks' => array('_asciidoc_display_feeds_menu_title_summary'),
    );

    $targets['asciidoc_menu_parent_guid'] = array(
      'name' => t('Menu link parent GUID'),
      'description' => t('The GUID for the menu entry parent.'),
      'callback'  => '_asciidoc_display_feeds_menu_parent_set_target',
      'form_callbacks' => array('_asciidoc_display_feeds_menu_parent_config_form'),
      'summary_callbacks' => array('_asciidoc_display_feeds_menu_parent_summary'),
    );

    $targets['asciidoc_menu_entry_weight'] = array(
      'name' => t('Menu link weight'),
      'description' => t('The weight for the menu entry'),
      'callback'  => '_asciidoc_display_feeds_menu_weight_set_target',
    );
  }

  if (module_exists('og_menu')) {
    $targets['asciidoc_menu_og_guid'] = array(
      'name' => t('OG Menu from GUID'),
      'description' => t('The Organic Groups Menu menu of the parent, from GUID.'),
      'callback'  => '_asciidoc_display_feeds_menu_og_set_target',
      'form_callbacks' => array('_asciidoc_display_feeds_menu_parent_config_form'),
      'summary_callbacks' => array('_asciidoc_display_feeds_menu_parent_summary'),
    );
  }

  $targets['asciidoc_content_type'] = array(
    'name' => t('AsciiDoc content type override'),
    'description' => t('Map this to the page type field, and in the settings, define the override for content type to use for chapter and book pages. All content types must have the same fields as the topic content type, which is the content type set in the main Processor options.'),
    'callback'  => '_asciidoc_display_feeds_content_type_set_target',
    'form_callbacks' => array('_asciidoc_display_feeds_content_type_config_form'),
    'summary_callbacks' => array('_asciidoc_display_feeds_content_type_summary'),
  );

  if (module_exists('path')) {
    $targets['asciidoc_alias'] = array(
      'name' => t('AsciiDoc path alias'),
      'description' => t('The URL alias for the item, with an optional prefix.'),
      'real_target' => 'path',
      'callback'  => '_asciidoc_display_feeds_alias_set_target',
      'form_callbacks' => array('_asciidoc_display_feeds_alias_config_form'),
      'summary_callbacks' => array('_asciidoc_display_feeds_alias_summary'),
    );
  }

  if (module_exists('translation')) {
    $targets['asciidoc_tnid'] = array(
      'name' => t('AsciiDoc translation source'),
      'description' => t('The content item this is a translation of, if any'),
      'callback'  => '_asciidoc_display_feeds_tnid_set_target',
    );
  }

  if (module_exists('comment')) {
    $targets['asciidoc_comment_settings'] = array(
      'name' => t('AsciiDoc comment settings'),
      'description' => t('Override the default comment settings for the content type'),
      'callback'  => '_asciidoc_display_feeds_comment_settings_set_target',
      'form_callbacks' => array('_asciidoc_display_feeds_comment_settings_config_form'),
      'summary_callbacks' => array('_asciidoc_display_feeds_comment_settings_summary'),
    );
  }

  return $targets;
}

/**
 * Sets the values for the AsciiDoc path alias field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_alias_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {

  // Code modified from path_feeds_set_target() in the Feeds module file
  // mappers/path.inc.

  $alias = ltrim(_asciidoc_display_get_value($values), '/');
  $language = isset($entity->language) ? $entity->language : language_default('language');
  $prefix = _asciidoc_display_feeds_replace_language($mapping['path_prefix'], $language);

  if ($alias) {
    $entity->path = array('alias' => $prefix . $alias, 'pathauto' => FALSE, 'language' => $language);
  }
}

/**
 * Returns the configuration form for the AsciiDoc path alias field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_alias_config_form(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('path_prefix' => '');

  return array(
    'path_prefix' => array(
      '#type' => 'textfield',
      '#title' => t('Prefix for path aliases'),
      '#description' => t('You can put [LANG] into your prefix to be replaced by the language code, if you map the language before this field in the feed mapping. Do not include the language URL prefix or domain added for non-default languages by the language system.'),
      '#default_value' => $mapping['path_prefix'],
    ),
  );
}

/**
 * Returns the configuration summary for the AsciiDoc path alias field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_alias_summary(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('path_prefix' => '');
  return t('Prefix: @path_prefix', array('@path_prefix' => $mapping['path_prefix']));
}

/**
 * Sets the values for the AsciiDoc translation ID field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_tnid_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {
  $tnid = _asciidoc_display_feeds_find_id(_asciidoc_display_get_value($values), 'nid');
  if ($tnid) {
    $source_node = node_load($tnid);
    if ($source_node) {
      $entity->translation_source = $source_node;
    }
  }
}

/**
 * Sets the values for the AsciiDoc content type field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_content_type_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {

  $content_type = FALSE;
  $value = _asciidoc_display_get_value($values);
  $changed = FALSE;
  if ($value == 'chapter' && isset($mapping['chapter'])) {
    $entity->type = $mapping['chapter'];
    $changed = TRUE;
  }
  elseif ($value == 'book' && isset($mapping['book'])) {
    $entity->type = $mapping['book'];
    $changed = TRUE;
  }
  if ($changed) {
    // Re-prepare the node if the type changed, and then set a few fields
    // that this process will override.
    $uid = $entity->uid;
    $log = $entity->log;
    node_object_prepare($entity);
    $entity->uid = $uid;
    $entity->log = $log;
  }
}

/**
 * Returns the configuration form for the AsciiDoc content type field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_content_type_config_form(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('chapter' => '', 'book' => '');

  // Bundle options code from FeedsProcessor::bundleOptions in the Feeds module.
  $options = array('' => t('No override'));
  foreach (field_info_bundles('node') as $bundle => $info) {
    if (!empty($info['label'])) {
      $options[$bundle] = $info['label'];
    }
    else {
      $options[$bundle] = $bundle;
    }
  }

  return array(
    'chapter' => array(
      '#type' => 'select',
      '#title' => t('Content type for chapter pages'),
      '#description' => t('Must have all the fields used in the mapping. Leave blank to not override default content type.'),
      '#default_value' => $mapping['chapter'],
      '#options' => $options,
    ),
    'book' => array(
      '#type' => 'select',
      '#title' => t('Content type for book pages'),
      '#description' => t('Must have all the fields used in the mapping. Leave blank to not override default content type.'),
      '#default_value' => $mapping['book'],
      '#options' => $options,
    ),
  );
}

/**
 * Returns the configuration summary for the AsciiDoc content type field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_content_type_summary(array $mapping, $target, array $form, array $form_state) {

  $mapping += array('chapter' => '', 'book' => '');
  $overrides = array();
  $bundles = field_info_bundles('node');

  if ($mapping['chapter']) {
    $overrides[] = t('Chapter content type: @chapter.', array('@chapter' => isset($bundles[$mapping['chapter']]['label']) ? $bundles[$mapping['chapter']]['label'] : $mapping['chapter']));
  }
  if ($mapping['book']) {
    $overrides[] = t('Book content type: @book.', array('@book' => isset($bundles[$mapping['book']]['label']) ? $bundles[$mapping['book']]['label'] : $mapping['book']));
  }
  if (!count($overrides)) {
    // If the summary callback returns empty, the form will not display.
    // A handy, undocumented feature of Feeds UI.
    $overrides[] = t('No overrides');
  }

  return implode(' ', $overrides);
}

/**
 * Sets the values for the AsciiDoc files field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_files_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {
  $language = isset($entity->language) ? $entity->language : language_default('language');
  $directory = file_build_uri(_asciidoc_display_feeds_replace_language($mapping['directory'], $language));
  file_prepare_directory($directory, FILE_CREATE_DIRECTORY + FILE_MODIFY_PERMISSIONS);

  foreach ($values as $filename) {
    $bare_name = basename($filename);
    file_unmanaged_copy($filename, $directory, $mapping['exists_behavior']);
  }
}

/**
 * Returns the configuration form for the AsciiDoc files field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_files_config_form(array $mapping, $target, array $form, array $form_state) {
  $mapping += array(
    'directory' => '',
    'exists_behavior' => FILE_EXISTS_REPLACE,
  );

  return array(
    'directory' => array(
      '#type' => 'textfield',
      '#title' => t('Directory for upload'),
      '#description' => t('Directory within the public files area to upload files to, ending in / . You can put [LANG] into your prefix to be replaced by the language code, if you map the language before this field in the feed mapping.'),
      '#default_value' => $mapping['directory'],
    ),
    'exists_behavior' => array(
      '#type' => 'select',
      '#title' => t('File exists behavior'),
      '#description' => t('What to do if a file with the same name already exists in this directory'),
      '#options' => array(
        FILE_EXISTS_REPLACE => t('Replace with new file'),
        FILE_EXISTS_RENAME => t('Add a unique numeric suffix to the file name'),
        FILE_EXISTS_ERROR => t('Leave the existing file as it is'),
      ),
    ),
  );
}

/**
 * Returns the configuration summary for the AsciiDoc files field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_files_summary(array $mapping, $target, array $form, array $form_state) {
  $mapping += array(
    'directory' => '',
    'exists_behavior' => FILE_EXISTS_REPLACE,
  );
  return t('Directory: @directory', array('@directory' => $mapping['directory']));
}

/**
 * Sets the values for the AsciiDoc menu link text field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_menu_title_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {
  // Code modified from https://www.drupal.org/sandbox/attisan/2144379

  $value = _asciidoc_display_get_value($values);
  $entity->menu['link_title'] = $value;
  if (!empty($mapping['menu_name']) || !isset($entity->menu['menu_name'])) {
    $entity->menu['menu_name'] = $mapping['menu_name'];
  }
  $entity->menu['enabled'] = 1;
  $entity->menu['description'] = '';
  if (!isset($entity->menu['weight'])) {
    $entity->menu['weight'] = 0;
  }
}

/**
 * Returns the configuration form for the AsciiDoc menu link text field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_menu_title_config_form(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('menu_name' => '');
  // The option of not setting the menu is needed for drupal.org import into
  // Organic Group menus.
  $options = array(
    '' => t('[Omit - set using some other method]'),
  ) + menu_get_menus();
  return array(
    'menu_name' => array(
      '#type' => 'select',
      '#title' => t('Menu to add links to'),
      '#options' => $options,
      '#description' => t('Must be set as a valid menu on the content type'),
      '#default_value' => $mapping['menu_name'],
    ),
  );
}

/**
 * Returns the configuration summary for the AsciiDoc menu link text field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_menu_title_summary(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('menu_name' => '');
  $labels = menu_get_menus();
  return t('Menu: @menu_name', array('@menu_name' => isset($labels[$mapping['menu_name']]) ? $labels[$mapping['menu_name']] : ''));
}

/**
 * Sets the values for the AsciiDoc menu parent field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_menu_parent_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {
  $mapping += array('remove_if_blank' => FALSE);

  // Code modified from https://www.drupal.org/sandbox/attisan/2144379
  $value = _asciidoc_display_feeds_find_id(_asciidoc_display_get_value($values));
  if ($value) {
    $entity->menu['plid'] = $value;
  }
  else {
    if ($mapping['remove_if_blank']) {
      unset($entity->menu);
    }
  }
}

/**
 * Returns the configuration form for the AsciiDoc menu parent fields.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_menu_parent_config_form(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('remove_if_blank' => FALSE);
  return array(
    'remove_if_blank' => array(
      '#type' => 'checkbox',
      '#title' => t('Remove if empty'),
      '#description' => t('If checked, and the parent GUID mapping field is empty, remove the menu information entirely. Map this field after all other menu-related fields if you use this option.'),
      '#default_value' => $mapping['remove_if_blank'],
    ),
  );
}

/**
 * Returns the configuration summary for the AsciiDoc menu parent fields.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_menu_parent_summary(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('remove_if_blank' => FALSE);
  if ($mapping['remove_if_blank']) {
    return t('Remove menu entry if field is empty');
  }
  else {
    return t('Do not remove menu entry if field is empty');
  }
}

/**
 * Sets the values for the AsciiDoc menu weight field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_menu_weight_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {
  // Code modified from https://www.drupal.org/sandbox/attisan/2144379
  $value = _asciidoc_display_get_value($values);
  if ($value) {
    $entity->menu['weight'] = $value;
  }
  else {
    $entity->menu['weight'] = 0;
  }
}

/**
 * Sets the values for the AsciiDoc OG menu field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_menu_og_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {
  $mapping += array('remove_if_blank' => FALSE);

  // Code modified from https://www.drupal.org/sandbox/attisan/2144379
  $value = _asciidoc_display_feeds_find_id(_asciidoc_display_get_value($values), 'og');
  if ($value) {
    $entity->menu['menu_name'] = $value;
  }
  else {
    if ($mapping['remove_if_blank']) {
      unset($entity->menu);
    }
  }
}

/**
 * Sets the values for the AsciiDoc path comment settings field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_comment_settings_set_target(FeedsSource $source, $entity, $target, array $values, array $mapping) {

  $mapping += array('comment_settings' => COMMENT_NODE_CLOSED);
  $entity->comment = $mapping['comment_settings'];
}

/**
 * Returns the configuration form for the AsciiDoc path comment settings field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_comment_settings_config_form(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('comment_settings' => COMMENT_NODE_CLOSED);

  return array(
    'comment_settings' => array(
      '#type' => 'select',
      '#title' => t('Override comment setting'),
      '#description' => t('You can map anything to this value. Only the setting is used, not the value.'),
      '#default_value' => $mapping['comment_settings'],
      '#options' => array(
        COMMENT_NODE_OPEN => t('Open'),
        COMMENT_NODE_CLOSED => t('Closed'),
        COMMENT_NODE_HIDDEN => t('Hidden'),
      ),
    )
  );
}

/**
 * Returns the configuration summary for the AsciiDoc comment settings field.
 *
 * Callback for asciidoc_display_feeds_feeds_processor_targets();
 */
function _asciidoc_display_feeds_comment_settings_summary(array $mapping, $target, array $form, array $form_state) {
  $mapping += array('comment_settings' => COMMENT_NODE_CLOSED);
  switch($mapping['comment_settings']) {
    case COMMENT_NODE_OPEN:
      return t('Set comments to open');

    case COMMENT_NODE_HIDDEN:
      return t('Set comments to hidden');

    default:
    case COMMENT_NODE_CLOSED:
      return t('Set comments to closed');
  }
}

/**
 * Finds an ID needed to set values for a node, given its GUID value in a feed.
 *
 * @param string $guid
 *   GUID value to find the ID for.
 * @param string $type
 *   (optional) Type of ID to find:
 *   - nid: Node ID.
 *   - (default) mlid: Menu link ID that this node has.
 *   - og: Organic Groups menu ID that this node has.
 *
 * @return int
 *   Node ID, menu link ID, or OG menu ID for the node with the given GUID from
 *   Feeds, or 0 if it is not found.
 */
function _asciidoc_display_feeds_find_id($guid, $type = 'mlid') {
  // Code modified from https://www.drupal.org/sandbox/attisan/2144379 .
  if (!$guid) {
    return 0;
  }

  $nid = db_select('feeds_item' , 'f')
    ->fields('f' , array('entity_id'))
    ->condition('f.guid', $guid)
    ->condition('entity_type', 'node')
    ->execute()
    ->fetchField();
  if (!$nid) {
    return 0;
  }

  if ($type == 'nid') {
    return $nid;
  }

  if ($type == 'og') {
    return 'menu-og-' . $nid;
  }

  if ($type == 'mlid') {
    $mlid = db_select('menu_links' , 'ml')
      ->fields('ml' , array('mlid'))
      ->condition('ml.link_path' , 'node/' . $nid)
      ->execute()
      ->fetchField();
    return $mlid;
  }

  // Unknown type.
  return 0;
}

/**
 * Gets a single value from the values array.
 *
 * @param array $values
 *   Array of values from feeds parsing.
 *
 * @return string
 *   The first non-empty value in $values, trimmed.
 */
function _asciidoc_display_get_value($values) {
  foreach ($values as $value) {
    $value = ltrim(trim($value), '/');
    if (strlen($value)) {
      return (string) $value;
    }
  }

  return '';
}

/**
 * Replaces [LANG] with the given language code in $text.
 *
 * @param string $text
 *   Text to do replacement in.
 * @param string $language
 *   Language code to replace [LANG] with.
 * @param bool $prefix
 *   If TRUE, treat this as a multilingual URL prefix.
 *
 * @return string
 *   The modified text.
 */
function _asciidoc_display_feeds_replace_language($text, $language, $prefix = FALSE) {
  $text = str_replace('[LANG]', $language, $text);

  if ($prefix && module_exists('locale') && drupal_multilingual()) {
    // In a multilingual site, if language detection is set to use a URL prefix,
    // add this as a prefix to $text. For instance, if the default language is
    // English, then Spanish URLs are all prefixed by /es but English URLs have
    // no prefix. This goes after the site's base path.

    $enabled = variable_get('language_negotiation_language', array());
    $uses_url = isset($enabled['locale-url']);
    $uses_prefix = variable_get('locale_language_negotiation_url_part', 500) == LOCALE_LANGUAGE_NEGOTIATION_URL_PREFIX;

    if ($uses_url && $uses_prefix) {
      $languages = language_list('enabled')[1];
      if (isset($languages[$language]) && !empty($languages[$language]->prefix)) {
        $base = base_path();
        if (!empty($base) && strpos($text, $base) === 0) {
          $text = $base . $languages[$language]->prefix . '/' . substr($text, strlen($base));
        }
        else {
          $text = $languages[$language]->prefix . '/' . $text;
        }
      }
    }
  }

  return $text;
}

/**
 * Finds and returns Feeds information for a node.
 *
 * @param object $node
 *   Node to find information for. The only components that need to be set
 *   are 'nid', 'language', and the field containing the source file.
 * @param bool $skip_file
 *   (optional) If set to TRUE, the source file and language are not read
 *   from the node. So, only the feed-wide information is returned.
 *
 * @return array|false
 *   An array of information about the feed for the node, or FALSE if it was
 *   not imported by Feeds. Array elements:
 *   - source_directory: Source file directory.
 *   - source_file: Source file, within the source directory.
 *   - language: Two-letter language code.
 *   - source_language: Two-letter language code of the source language.
 *   - translated: TRUE if this book is translated; FALSE if not.
 *   - block_message: Message for the source editing block, if configured.
 *   - image_dir: Directory for images for this node.
 */
function _asciidoc_display_feeds_get_feeds_info($node) {
  if (!$node || !isset($node->nid)) {
    return FALSE;
  }

  $info = feeds_item_info_load('node', $node->nid);
  if (!$info || !isset($info->id)) {
    return FALSE;
  }
  $importer = feeds_importer($info->id);
  if (!$importer) {
    return FALSE;
  }

  // Read the AsciiDocFetcher configuration from the importer.
  $config = $importer->fetcher->getConfig();
  if (!$config || empty($config['source_directory']) || empty($config['source_field']) || !isset($config['translated']) || !isset($config['source_language']) || !isset($config['source_message'])) {
    return FALSE;
  }

  $source_field = $config['source_field'];
  $source_file = '';
  if (isset($node->$source_field)) {
    $stuff = $node->$source_field;
    if (isset($stuff['und'][0]['value'])) {
      $source_file = $stuff['und'][0]['value'];
    }
  }

  $info = array();
  $info['source_directory'] = $config['source_directory'];
  $info['source_file'] = $source_file;
  $info['language'] = $node->language;
  $info['source_language'] = $config['source_language'];
  $info['translated'] = $config['translated'];
  $info['block_message'] = $config['source_message'];

  // Read the AsciiDocParser configuration from the importer.
  $config = $importer->parser->getConfig();
  if (!$config || empty($config['internal_image_prefix'])) {
    return FALSE;
  }

  $image_dir = rtrim(_asciidoc_display_feeds_replace_language($config['internal_image_prefix'], $info['language']), '/');
  $info['image_dir'] = $image_dir;

  return $info;
}
