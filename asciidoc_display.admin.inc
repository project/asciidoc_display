<?php

/**
 * @file
 * Administrative pages for AsciiDoc display.
 */

/**
 * Page callback: Outputs the main administrative page for AsciiDoc Display.
 */
function asciidoc_display_main_admin() {

  $build = array();

  $clear = l(t('Clear book cache'), 'admin/config/development/asciidoc/clear_cache');

  $build['clear'] = array(
    '#markup' => '<p>' . $clear . '</p>',
  );

  return $build;
}

/**
 * Page callback: Clears the cache for this module.
 */
function asciidoc_display_clear_cache_page() {
  asciidoc_display_clear_calculation_caches();
  drupal_set_message(t('Book cache cleared'));
  drupal_goto('admin/config/development/asciidoc');
}
