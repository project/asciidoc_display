INTRODUCTION
------------

This project contains scripts and a set of Drupal modules that will allow you to
use the Asciidoc documentation formatting system to generate documentation, and
then import or display it in a Drupal site. It also supports on-line source
editing with preview, and downloading either an updated source file or a unified
diff.

The project contains a base module, which is needed for all Drupal
functionality. Then, you'll need to choose a sub module for display:

* Direct: Displays AsciiDoc output directly.

* Feeds: Provides Feeds module integration so you can import AsciiDoc output
  into your site.

You will need to install the base module and either Feeds or Display module like
you would any other Drupal module. But in order for it to work, depending on
which pieces of the functionality you want to use, there are additional
steps. See below.


REQUIREMENTS - GENERAL
----------------------

You will need to have the multi-byte extension enabled in PHP, which is most
likely true anyway.


ASCIIDOC OUTPUT BUILD SCRIPTS
-----------------------------

To use this module to display an AsciiDoc book directly or import using Feeds,
you will need to generate some XHTML output from AsciiDoc source files. For
that, you will need several open-source tools:

- AsciiDoc: http://asciidoc.org/INSTALL.html
- DocBook: http://docbook.org or http://www.dpawson.co.uk/docbook/tools.html

On a Linux machine, you can use one of these commands to install the tools:
  apt-get install asciidoc docbook
  yum install asciidoc docbook

On a Mac:
  brew install asciidoc xmlto
  echo "export XML_CATALOG_FILES=/usr/local/etc/xml/catalog" >> ~/.bash_profile
  source ~/.bash_profile

Note that these scripts do not work with all available versions of AsciiDoc
and Docbook tools. They have been tested to work with:
asciidoc - version 8.6.9
xmlto - version 0.0.25

You can check versions by typing
  asciidoc --version
  xmlto --version
on the command line.

Once you have the tools installed, the "mkoutput.sh" script in the
"sample_scripts" directory is a starting point for making XHTML output suitable
for display in this module. Note that this module does rely on some
customizations in the "bare.xsl" style sheet in order to display the XHTML
output files correctly.

The DocBook XSL stylesheet files can be downloaded from
  http://sourceforge.net/projects/docbook/
[Look around to make sure you are downloading the style sheet files
(docbook-xsl) and not the documentation.] In that download, the "xhtml"
directory has the xsl stylesheets that the xmlto command uses by default to make
its output from the DocBook source. The bare.xsl stylesheet included in this
project contains some overrides; if you are unhappy with the output, you may
want to do more. You might also look at the slightly more refined scripts that
the User Guide project uses, which also make e-book and PDF output:
  https://www.drupal.org/project/user_guide

NOTE: If you are building output for Feeds import, you may wish to change the
output style slightly. The default (for direct display) is that the chapter
pages display the first topic of the chapter. You will probably want the topics
to start on their own pages instead, so that the chapter imports are kept clean,
and each topic has its own import page. To do that, add this to the xmlto
commands:
  --stringparam chunk.first.sections=1


MODULE CONFIGURATION - DIRECT DISPLAY
-------------------------------------

If you are using the direct display sub-module to display your output, the first
step is to generate some bare XHTML output from the scripts (see above). Then
visit the configuration page at
  admin/config/development/asciidoc
and set up a "book". Configuration notes:
- Choose a display URL for each book.
- Point the book to the directory where the HTML output has been generated
  from the scripts. If the book is translated, output from the individual
  translations should be below this in directories 'en', 'de', 'es', etc.
  corresponding to the standard ISO language codes. You can also have one
  level of image directory below the main output directory, with an arbitrary
  name.
- Project information is for informational purposes, to point viewers and
  editors to the project where downloads and source might exist (it just makes
  a link).
- A File an Issue link can also be provided for viewers.
- If you want to enable on-line editing, provide a source directory, which can
  be read-only (and see the section below about additional downloads for
  editing). Like the output directory, this is organized by languages if the
  book is translated. The side-by-side translation editing feature will only
  work if the source file names are the same in each of these subdirectories.

You'll also need to set up View and Edit permissions.

Note: This module only supports file names for source and output files and image
directories that contain upper- and lower-case letters, numbers, and - _ and
. characters. No other characters are allowed.


USING FEEDS TO IMPORT
---------------------

The AsciiDoc Display Feeds module provides Feeds integration for AsciiDoc output
import. Suggestions and notes:

* Some Feeds imports will not work correctly if the user running them has
insufficient permissions. So, it is advisable to run the imports as a user
that has permission to create, edit, and delete content of the type that you're
importing; administer URL aliases; and administer menus (depending on what Feeds
field mappings you are using). You can set the user ID via Drush, if you are not
using the Feeds import UI.

* You will need to use the provided AsciiDoc Fetcher and AsciiDoc Parser when
you set up your feed. There are also a few special mappers, including some for
creating menu items, which have options to aid in mapping AsciiDoc parsed fields
to content fields.

* There is a GUID field provided, which makes a unique combination of a prefix
(which you need to configure in Feeds and make unique per AsciiDoc book), the
language, and the output HTML file name each page is read from.

* URL aliases -- use a prefixed "Output file" field as the URL alias of each
created content item. Then put the same prefix on internal links. The AsciiDoc
Parser and AsciiDoc path alias mapper have settings for this prefix.

* Menu items -- the provide Menu mappers can be used to add the imported items
to a menu. You will need to make sure to define the menu name on the menu link
text field. Mapping the parent GUID and weight will give you a hierarchical
menu, although you may need to import twice to get it to work, because the menu
hierarchy may not work out the first time.

* Make sure the text format you use does not have the "Convert line breaks into
HTML" filter turned on. AsciiDoc output has line breaks inside HTML paragraphs.

* Images: Image tags pointing to local images in each file can be imported
into an image tag, and you can add a prefix to your image tag URLs. This will
work OK the first time you import a page. However, the next time you import, the
File/Image module will add a suffix to your image file name (for instance, if
the file was myimage.png, it will become myimage_0.png, because myimage.png
already exists). Then the original imported image will be deleted, causing your
image tags to break. The solution to this is to not use Feeds to import
images. Place them somewhere using a separate process, and then use the prefix
functionality to add a suitable prefix to the image tags' URLs.


CACHE
-----

These modules make use of the Drupal cache for efficiency. They should be
invalidating the data when necessary, but if you ever find that your site
is not behaving as you think it should, clear the cache.


BLOCKS
------

This project's Display module comes with several blocks:
* The "book list" block lists all of the defined books. This block can be
  displayed anywhere.
* The "navigation" block is specific to each book and visible only on displayed
  book pages. It shows the table of contents.
* The "source information" block shows information about the book, and allows
  appropriate users to edit the source files.
* The "languages" block is only visible when viewing a translated book, and it
  lists all available translations in a list. It doesn't integrate with the
  Drupal UI translation system (for now).

The Feeds module comes with one block: the "edit source" block lets users edit
the AsciiDoc source of a content item that was imported. Display it on a content
item page. You will need to have entered the source directory in the AsciiDoc
Fetcher configuration for each book you imported. You will also need to save the
source file information, using a mapper, in your feed, and configure the field
name on the Fetcher.


COPYRIGHT
---------

Your AsciiDoc output can include a copyright notice, which will be displayed at
the bottom of every generated page if it exists (direct display method), and
imported as part of the item body (feeds method).

To make this happen, add a role attribute of "copyright" to a paragraph in your
document. Example:

[role="copyright"]
Copyright notice: Copyright 2015-2016 by the individual contributors; see
<<copyright>> for details. Licensed under
https://creativecommons.org/licenses/by-sa/2.0/[CC BY-SA 2.0].

You'll need to look at the HTML output files and find the file that contains
this notice; put this file name into the book configuration.


SUMMARY PARAGRAPHS
------------------

Each book, chapter, and topic can have a summary paragraph in it. For example:

[role="summary"]
Guide for how to manage the project, and starting new phases. Checklists and
issue templates for each phase. Topic templates. Notes and lessons learned.

These paragraphs will be removed from direct AsciiDoc output. In Feeds output,
they are returned so they can be saved in fields on the imported content
items. HTML is stripped from summaries for Feeds.


INSTALLATION - EDITING
----------------------

If you want to use the AsciiDoc editor and preview functionality, you will need
to download some additional libraries and files. If you just want to display
AsciiDoc output in your site or import it, you do not need these downloads.

Note that the editor does not affect the Drupal site at all, or edit the source
files directly. It just reads in the source files and lets the user download the
result as a patch or replacement text file. It also has an option to edit
two languages side-by-side, if the book is translated.

NOTE: Some of the downloads, when extracted, have incorrect file
permissions. Make sure all files can be read by the web user!

markItUp!
http://markitup.jaysalvat.com/home/
Download and install so that you have a file
sites/all/libraries/markitup/markitup/jquery.markitup.js
as well as the rest of the files in the download.

AsciiDoctor-Markitup images
https://github.com/lordofthejars/asciidoctor-markitup
(See also:
http://www.lordofthejars.com/2013/07/asciidoc-editor-with-markitup.html )
Download the images from the markitup/sets/asciidoc/images directory and
place them in
sites/all/libraries/asciidoctor_images

You'll also need to copy the contents of the asciidoctor_extra directory into
sites/all/libraries/asciidoctor_images (two files: asciidoc_display.edit.css and
monospace.png).

AsciiDoctor JS
https://github.com/asciidoctor/asciidoctor.js
(See also: http://asciidoctor.org/ )
Download and install so that you have a file
asciidoctor/dist/ascidoctor-all.min.js
as well as the rest of the files in that directory of the download.

Check before downloading that the "dist" directory exists. You may need to go
back to a release called 1.5.4-rc.1 (dated Jan 16, 2016) to get this in the
download.


INSTALLATION - DIFF FILES
-------------------------

If you want users who edit files to be able to download patches, you will need
to install the PECL xdiff extension for PHP on the web server where your Drupal
site is hosted. However, since the module only generates generic patch files,
not Git diff files, for most projects it is probably better for them just to
download the updated text file and make a patch using Git.

Note that patch files do not work with the AsciiDoc Feeds module, at least at
the present time. People can just downloaded the updated text files.

To install PECL xdiff, see:
  http://php.net/manual/en/install.pecl.php
  https://pecl.php.net/package/xdiff

On a Linux machine, if you already have PECL, in theory you should be able to
type:
  pecl install xdiff
to install it. You will also need to have the "libxdiff" Linux package installed
first. You can learn how This does not seem to be provided by Ubuntu, and I
don't know about other distributions. Try an internet search, or the libxdiff
instructions here:
  http://www.wikidot.org/doc:debian-quickstart
I could not get this to work on Ubuntu, however -- the PECL part failed with
compilation errors.

So, the diff making feature of this module is untested. Use at your own risk!
