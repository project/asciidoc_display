<?php

/**
 * @file
 * AsciiDoc utility functions for sub-modules to use.
 */

/**
 * Checks to see if a discovered filename and language are valid.
 *
 * @param string $language
 *   Language code, validated to be a known language code.
 * @param string $filename
 *   (optional) File name, validated to not contain any directory information
 *   or bad characters. The only characters allowed are letters, numbers, and
 *   the - _ and . characters.
 *
 * @return bool
 *   TRUE if the language and file name are valid, and FALSE if not.
 */
function asciidoc_display_validate($language, $filename = '') {
  if ($language !== '') {
    include_once DRUPAL_ROOT . '/includes/iso.inc';
    $all_languages = _locale_get_predefined_list();
    if (!isset($all_languages[$language])) {
      return FALSE;
    }
  }

  if ($filename && preg_match('/[^a-zA-Z0-9.\-_]/', $filename)) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Makes a list of the languages available in a book.
 *
 * Tacitly assumes the book is translated.
 *
 * @param string $base_dir
 *   The base directory for the book.
 * @param string $type
 *   What to return, one of the following:
 *   - options: (default) Make an options list for a select element.
 *   - links: Make a list suitable for theme_links().
 * @param string $base_path
 *   (optional) If $type is 'links', the links will point to
 *   "$base_path/$langcode/index.html".
 *
 * @return array
 *   List showing the languages that actually exist for this book. Empty if
 *   there are no recognizable language subdirectories.
 */
function asciidoc_display_list_languages($base_dir, $type = 'options', $base_path = '') {
  // See if it's in the cache.
  $key = $base_dir . '-' . $type . '-' . $base_path;
  $cached = cache_get('asciidoc_display_list_languages');
  if ($cached) {
    $cached = $cached->data;
  }
  else {
    $cached = array();
  }
  if (isset($cached[$key])) {
    return $cached[$key];
  }

  // It's not in the cache, so calculate it.

  include_once DRUPAL_ROOT . '/includes/iso.inc';
  $all_languages = _locale_get_predefined_list();

  $found = array();
  $dir = drupal_realpath($base_dir);
  if($handle = opendir($dir)) {
    while (($entry = readdir($handle)) !== FALSE) {
      if (isset($all_languages[$entry]) && is_dir($dir . '/' . $entry)) {
        $language = $all_languages[$entry];
        // This is an array with the English name followed optionally by
        // the name in the native language.
        $name = (count($language) > 1) ? $language[1] : $language[0];
        if ($type == 'options') {
          $found[$entry] = $name;
        }
        else {
          $found[$entry] = array(
            'title' => $name,
            'href' => $base_path . '/' . $entry . '/index.html',
          );
        }
      }
    }
  }

  $cached[$key] = $found;
  cache_set('asciidoc_display_list_languages', $cached);
  return $found;
}

/**
 * Finds the copyright notice.
 *
 * @param string copyright_file
 *   File that should contain the copyright notice.
 *
 * @return string
 *   Copyright information to display, or an empty string if not found.
 */
function asciidoc_display_find_copyright($copyright_file) {
  if (!$copyright_file) {
    return '';
  }

  $path = drupal_realpath($copyright_file);
  $html = @file_get_contents($path);
  if (!$html) {
    return '';
  }

  $html_info = asciidoc_display_parse_bare_html($html);
  return isset($html_info['copyright']) ? $html_info['copyright'] : '';
}

/**
 * Finds and parses the table of contents into a parents list.
 *
 * @param string index_file
 *   File that should contain the full table of contents.
 *
 * @return array
 *   Associative array where the keys are file names, and the values are
 *   arrays giving the parent file names (key: file) and weights (key: weight)
 *   in the table of contents hierarchy. The index file is also included, but
 *   its parent is an empty string and weight is zero. Empty array if it
 *   cannot be calculated.
 */
function asciidoc_display_list_parents($index_file) {
  if (!$index_file) {
    return array();
  }

  $path = drupal_realpath($index_file);
  $html = @file_get_contents($path);
  if (!$html) {
    return array();
  }

  $html_info = asciidoc_display_parse_bare_html($html, array('parse_parents' => TRUE));
  return isset($html_info['parents']) ? $html_info['parents'] : array();
}

/**
 * Builds information for other languages, assuming the book is translated.
 *
 * @param string $file
 *   The particular file name within the book.
 * @param string $source_dir
 *   The system path to the source directory.
 * @param string $current_langcode
 *   The language code of the current editing page.
 * @param string $source_langcode
 *   The language code of the source language for the book.
 * @param string $path_prefix
 *   Path prefix for editing links. The links will be made to
 *   "$path_prefix/$langcode/$file".
 *
 * @return
 *   Build array containing a fieldset containing the other language links,
 *   or NULL if there are no other languages. Also contains a text box with
 *   the source langauge in another fieldset, if it is different from the
 *   current language.
 */
function asciidoc_display_edit_other_languages($file, $source_dir, $current_langcode, $source_langcode, $path_prefix) {
  $build = array();
  $languages = asciidoc_display_list_languages($source_dir);

  // Make list of links to other languages.
  if ($path_prefix) {
    $links = array();
    foreach ($languages as $langcode => $name) {
      if ($langcode != $current_langcode) {
        $links[$langcode] = array(
          'title' => $name,
          'href' => $path_prefix . '/' . $langcode . '/' . $file,
          'attributes' => array('target' => '_blank'),
        );
      }
    }
    if (count($links)) {
      $build['language'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Other languages'),
        '#description' => t('View/edit a different language. Opens in a new window.'),
      );

      $build['language']['links'] = array(
        '#theme' => 'links__asciidoc_display_languages',
        '#links' => $links,
        '#attributes' => array('class' => array('asciidoc_display_languages_edit')),
      );
    }
  }

  // Make text box with source language, if different from current language.
  if ($source_langcode != $current_langcode) {
    $text = _asciidoc_display_load_source($file, $source_dir, TRUE, $source_langcode);
    if ($text) {
      $build['original'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Original language'),
      );

      $build['original']['source_text'] = array(
        '#type' => 'textarea',
        '#title' => t('Text in @language', array('@language' => $languages[$source_langcode])),
        '#value' => $text,
        '#rows' => 20,
        '#cols' => 80,
      );
    }
  }

  return $build;
}

/**
 * Form constructor for the AsciiDoc editing form.
 *
 * @parm string $file
 *   File name being edited.
 * @param string $source_dir
 *   The system path to the source directory.
 * @param bool $translated
 *   TRUE if the book has multiple languages, in which case the files are
 *   assumed to be in $source_dir within language directories. FALSE if not,
 *   in which case the files are directly in $source_dir.
 * @param string $langcode
 *   Language code, if there is one.
 * @param string $source_langcode
 *   The language code of the source language for the book.
 * @param string $path_prefix
 *   Path prefix for editing links for other langauges. The links will be made
 *   to "$path_prefix/$langcode/$file" if the book is translated.
 * @param bool $do_patches
 *   TRUE if submitting the form should download a patch file; FALSE if it
 *   should download the edited source file.
 * @param string $line_endings
 *   Type of line endings to use: 'unix' for Unix/Linux; anything else for
 *   Windows.
 * @param string $image_url
 *   URL for image directory.
 */
function asciidoc_display_edit_form($form, &$form_state, $file, $source_dir, $translated, $langcode, $source_langcode, $path_prefix, $do_patches, $line_endings, $image_url) {
  $form['#asciidoc_source_file'] = $file;
  $form['#asciidoc_do_patches'] = $do_patches;
  $form['#asciidoc_line_endings'] = $line_endings;
  $form['#asciidoc_source_dir'] = $source_dir;

  $text = _asciidoc_display_load_source($file, $source_dir, $translated, $langcode);
  if (!$text) {
    $form['error'] = array(
      '#type' => 'markup',
      '#markup' => t('Could not read %file', array('%file' => $file)),
    );
    return $form;
  }

  if ($translated) {
    $other = asciidoc_display_edit_other_languages($file, $source_dir, $langcode, $source_langcode, $path_prefix);
    if ($other) {
      $form['other_language'] = $other;
    }
  }

  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('You are editing the source for a page that is formatted using the <a href="http://asciidoc.org">AsciiDoc markdown format</a>. After editing, download your revised source and attach it to an issue.') . '</p><p>' . t('There is a Preview button in the toolbar. It will show you the formatting output, except that cross links do not show up with topic titles as the link text.') . '</p>',
  );


  $form['editor'] = array(
    '#type' => 'textarea',
    '#title' => t('Edit %file', array('%file' => $file)),
    '#default_value' => $text,
    '#rows' => 20,
    '#cols' => 80,
    '#attributes' => array('class' => array('mark-it-up')),
  );

  $form['#attached']['library'][] = array('asciidoc_display', 'asciidoc_display.edit');
  $form['#attached']['js'][] = array(
    'data' => array(
      'asciidoc_display' => array(
        'image_dir' => $image_url,
      ),
    ),
    'type' => 'setting',
  );

  if ($do_patches) {
    $label = t('Download patch file');
  }
  else {
    $label = t('Download new file');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $label,
  );

  return $form;
}

/**
 * Form submission handler for asciidoc_display_edit_form().
 */
function asciidoc_display_edit_form_submit($form, &$form_state) {
  $file = $form['#asciidoc_source_file'];
  $text = $form_state['values']['editor'];
  $line_endings = $form['#asciidoc_line_endings'];
  $source_dir = $form['#asciidoc_source_dir'];
  $do_patches = $form['#asciidoc_do_patches'];

  // The text editor prefers DOS line endings. Convert back to Unix, if
  // requested.
  if ($line_endings == 'unix') {
    $text = str_replace("\r", '', $text);
  }

  if (!$text) {
    drupal_set_message(t('No text in editor'));
    return;
  }

  // Make a patch file, optionally.
  if ($do_patches) {
    $path = drupal_realpath($source_dir . '/' . $file);
    $old_text = @file_get_contents($path);
    if ($old_text) {
      $text = xdiff_string_diff($old_text, $text);
    }

    // If we made a diff, change the filename.
    $file = $file . '.patch';
  }

  // Send the patch or text file to the browser.
  drupal_add_http_header('Content-Type', 'text/plain');
  drupal_add_http_header('Content-Disposition', format_string('attachment; filename="@filename"', array('@filename' => $file)));
  print $text;
  exit();
}

/**
 * Loads AsciiDoc source text.
 *
 * @param string $file
 *   The particular file name within the book.
 * @param string $source_dir
 *   The system path to the source directory.
 * @param bool $translated
 *   TRUE if the book has multiple languages, in which case the files are
 *   assumed to be in $source_dir within language directories. FALSE if not,
 *   in which case the files are directly in $source_dir.
 * @param string $langcode
 *   The language code to load (ignored if not translated).
 *
 * @return string|false
 *   The text of the file, or FALSE if invalid language, invalid file, or
 *   not found.
 */
function _asciidoc_display_load_source($file, $source_dir, $translated, $langcode) {
  if (!asciidoc_display_validate($langcode, $file)) {
    return FALSE;
  }

  $language_path = $translated ? '/' . $langcode : '';
  $path = drupal_realpath($source_dir . $language_path . '/' . $file);
  $text = @file_get_contents($path);
  return $text;
}

/**
 * Parses a "bare" HTML output from AsciiDoc.
 *
 * Assumes the bare HTML file was made with the scripts/bare.xsl style sheet.
 *
 * @param string $html
 *   HTML to parse. Assumed not to be empty.
 * @param array $options
 *   Associative array of options for parsing, with (all optional) elements:
 *   - keep_toc: TRUE if this page should display the table of contents. If
 *     FALSE (default), the table of contents is removed from the page. In
 *     either case, the table of contents is returned in the return array.
 *  - toc_active: Add an "active" class to table of contents links pointing to
 *    this location, if it is provided. If not provided, the HTML for the table
 *    of contents is left as it was.
 *  - highlight_php: Apply standard PHP syntax highlighting for PHP code, if
 *    set to TRUE. (Default is FALSE.)
 * - link_prefix: If provided, add this prefix to all link URLs that point to
 *   pages within the book. (Default is empty.)
 * - image_prefix: If provided, add this prefix to image source URLs that point
 *   to local images.
 * - parse_parents: If set to TRUE (FALSE is the default), parse the table of
 *   contents (assumed to be called on the full index.html file) and make an
 *   array of parents in the hierarchy.
 * - remove_h1_titles: If set to TRUE (FALSE is the default), remove all H1
 *   elements with class "title" in the document (to avoid duplication with the
 *   page title).
 * - remove_h2_titles: If set to TRUE (FALSE is the default), remove all H2
 *   elements with class "title" in the document (to avoid duplication with the
 *   page title).
 * - remove_source_line: If set to TRUE (FALSE is the default), after locating
 *   the line giving the source file, remove it from the output.
 *
 * @return array
 *   Associative array containing:
 *   - book_title: The title of the book this is in.
 *   - section_title: The title of this particular topic/section/page.
 *   - body: The HTML of the page, without the titles and table of contents.
 *     Exception: On the home page, the table of contents is displayed.
 *   - toc: The HTML for the table of contents section, if any.
 *   - parents: The parents array for the table of contents section, if any.
 *     This is an associative array of file name => info, and info has keys
 *     parent (parent file name) and weight. Only present if $parse_parents is
 *     TRUE.
 *   - files: Array of the source files for the page, if any are noted. These
 *     are validated to contain only legal characters.
 *   - copyright: The HTML for the copyright notice, if any.
 *   - images: Array of 'src' attributes of images found in the page.
 *   - type: Type of page, either 'book', 'chapter', or 'topic'.
 *   - summary: The summary of the page, if it was set.
 */
function asciidoc_display_parse_bare_html($html, $options = array()) {
  $options += array(
    'keep_toc' => FALSE,
    'toc_active' => '',
    'highlight_php' => FALSE,
    'link_prefix' => '',
    'image_prefix' => '',
    'parse_parents' => FALSE,
    'remove_h1_titles' => FALSE,
    'remove_h2_titles' => FALSE,
    'remove_source_line' => FALSE,
  );

  // This function may get called several times on the same HTML if blocks
  // are in use, so put the output into drupal_static(), and check to see
  // if we have already calculated it.
  $found = &drupal_static(__FUNCTION__);
  $args = func_get_args();
  $key = md5(serialize($args));
  if (isset($found[$key])) {
    return $found[$key];
  }

  // Also, use the cache for longer-term storage.
  $cached = cache_get('asciidoc_display_parse_bare_html');
  if ($cached) {
    $cached = $cached->data;
  }
  else {
    $cached = array();
  }
  if (isset($cached[$key])) {
    $found[$key] = $cached[$key];
    return $cached[$key];
  }

  // Parsed output was not found in local storage or the cache, so we need
  // to parse it.
  $dom = new \DomDocument('1.0', 'UTF-8');
  $dom->loadHTML($html);

  // Read off the book title.
  $elem = $dom->getElementById('asciidoc-display-book-title');
  $info['book_title'] = $elem->nodeValue;

  // Read off the section title.
  $elem = $dom->getElementById('asciidoc-display-section-title');
  $info['section_title'] = $elem->nodeValue;

  // Read out and optionally remove the source files. These look like:
  // <span class="remark">Source file: config.txt</span>
  // Also, read out and remove the copyright notice. The old style for this
  // looks like: <span class="remark">Copyright notice: ...</span>.
  // For the new style, see below.
  $info['files'] = array();
  $info['copyright'] = '';
  $to_remove = [];
  foreach ($dom->getElementsByTagName('span') as $span) {
    if ($span->getAttribute('class') == 'remark') {
      $val = $span->nodeValue;
      $matches = array();
      if (preg_match('/^Source file: ([a-zA-Z_.0-9\-]+)$/', $val, $matches)) {
        $info['files'][] = $matches[1];
        if ($options['remove_source_line']) {
          $to_remove[] = $span;
        }
      }
      else {
        $full_span = $dom->saveHTML($span);
        // This is the old style of copyright notice, and is only OK for
        // English.
        if (preg_match('/Copyright notice: /i', $full_span)) {
          $span->removeAttribute('class');
          $info['copyright'] = $dom->saveHTML($span);
          $to_remove[] = $span;
        }
      }
    }
  }
  // Remove after first loop, so the DOMNodeList being iterated over remains
  // unmodified.
  foreach ($to_remove as $span) {
    // This is enclosed in an EM in a P tag. Remove it. We will display it on
    // every page instead.
    $pparent = $span->parentNode->parentNode;
    $pparent->parentNode->removeChild($pparent);
  }

  // Read out the new-style copyright notice and summary. These are
  // paragraph tags with a role attribute of "summary" or "copyright".
  $info['summary'] = '';
  $paragraphs = $dom->getElementsByTagName('p');
  foreach ($paragraphs as $paragraph) {
    $role = $paragraph->getAttribute('class');
    if ($role == 'summary' || $role == 'copyright') {
      // Remove the class. Remove the paragraph. Save it for return.
      $paragraph->removeAttribute('class');
      $info[$role] = ($role == 'copyright') ? $dom->saveHTML($paragraph) : $paragraph->nodeValue;
      $paragraph->parentNode->removeChild($paragraph);
    }
  }

  // Locate the image tags and record their source locations.
  $images = $dom->getElementsByTagName('img');
  $info['images'] = array();
  foreach ($images as $image) {
    $info['images'][] = $image->getAttribute('src');
  }

  // Modify the links and image sources.
  _asciidoc_display_prefix_links($dom, 'a', 'href', $options['link_prefix']);
  _asciidoc_display_prefix_links($dom, 'img', 'src', $options['image_prefix']);

  // Remove h1 titles, if requested.
  if ($options['remove_h1_titles']) {
    $h1s = $dom->getElementsByTagName('h1');
    foreach ($h1s as $h1) {
      if ($h1->getAttribute('class') == 'title') {
        $h1->parentNode->removeChild($h1);
      }
    }
  }

  // Remove h2 titles, if requested.
  if ($options['remove_h2_titles']) {
    $h2s = $dom->getElementsByTagName('h2');
    foreach ($h2s as $h2) {
      if ($h2->getAttribute('class') == 'title') {
        $h2->parentNode->removeChild($h2);
      }
    }
  }

  // Read off and/or remove the table of contents div, if there is one; it has
  // class "toc". Also locate the main content div, which has class
  // "asciidoc-display-main-content". And try to figure out what type of
  // page it is, based on the presence of divs of certain classes.
  $toc = NULL;
  $body = NULL;
  $has_chapter = FALSE;
  $has_topic = FALSE;
  $divs = $dom->getElementsByTagName('div');
  foreach ($divs as $div) {
    $class = $div->getAttribute('class');
    if (!$class) {
      continue;
    }
    $classes = array_filter(explode(' ', $class));

    // See if this is a chapter or topic page.
    $chapter_types = array('chapter', 'preface', 'appendix', 'index', 'glossary');
    foreach ($chapter_types as $ch_class) {
      if (in_array($ch_class, $classes)) {
        $has_chapter = TRUE;
        break;
      }
    }
    if (in_array('section', $classes)) {
      $has_topic = TRUE;
    }

    // Pick out or remove the table of contents.
    if (in_array('toc', $classes)) {
      if ($options['keep_toc']) {
        $toc = $div;
        // Add a class denoting that this is a table of contents that is
        // present on the page.
        $classes[] = 'asciidoc-home-toc';
        $toc->setAttribute('class', implode(' ', $classes));
      }
      else {
        $toc = $div->parentNode->removeChild($div);
      }
    }

    // Pick out the main body div.
    if (in_array('asciidoc-display-main-content', $classes)) {
      $body = $div;
    }
  }

  // Figure out the page type, based on the div classes found in the loop
  // above.
  if ($has_chapter) {
    $info['type'] = 'chapter';
  }
  elseif ($has_topic) {
    $info['type'] = 'topic';
  }
  else {
    $info['type'] = 'book';
  }

  if ($toc && $options['parse_parents']) {
    // Parse UL in the table of contents into a parents array.
    $children = $toc->childNodes;
    foreach ($children as $child) {
      if ($child->tagName == 'ul') {
        $info['parents'] = _asciidoc_display_calculate_parents($child, 'index.html', $options['link_prefix']);
        break;
      }
    }
  }

  if ($toc && $options['toc_active']) {
    // Set any links in the table of contents to this URL to have class
    // "active", and their UL/LI parents to "active-trail".
    $links = $toc->getElementsByTagName('a');
    foreach ($links as $link) {
      $href = $link->getAttribute('href');
      if (strpos($href, $options['toc_active']) !== FALSE) {
        _asciidoc_display_link_set_active($link);
      }
    }
  }

  if ($options['highlight_php']) {
    // Yes, we do need to call this several times. Sigh.
    _asciidoc_display_highlight_xml_once($body, $dom);
    _asciidoc_display_highlight_xml_once($body, $dom);
    _asciidoc_display_highlight_xml_once($body, $dom);
    _asciidoc_display_highlight_xml_once($body, $dom);
  }

  $body = _asciidoc_display_dom_element_to_html($body);
  $toc = _asciidoc_display_dom_element_to_html($toc);

  $info['body'] = $body;
  $info['toc'] = $toc;

  $found[$key] = $info;
  $cached[$key] = $info;
  cache_set('asciidoc_display_parse_bare_html', $cached);
  return $info;
}

/**
 * Recursively calculates the parents array from a table of contents div.
 *
 * @param DomElement $toc
 *   Table of contents to look through, which should be a UL element.
 * @param string $parent
 *   Parent file name for this part of the table of contents.
 * @param string $link_prefix
 *   Link prefix that may be prepended on href attributes of links, to strip
 *   off.
 * @param array $parents
 *   In-progress array of parents, if this is a recursive call.
 *
 * @return array
 *   Associative array whose keys are file names, and whose values are arrays
 *   with the file name of the parent in the table of contents (key: file), and
 *   the weight at this level of the table of contents (key: weight).
 */
function _asciidoc_display_calculate_parents($toc, $parent, $link_prefix, $parents = array()) {
  if (!isset($parents[$parent])) {
    $parents[$parent] = array('file' => '', 'weight' => 0);
  }

  $children = $toc->childNodes;
  $weight = -1;
  foreach ($children as $li) {
    $weight++;
    // Each LI should have an A link, and optionally a UL for another
    // level of table of contents.
    $li_children = $li->childNodes;
    $ul_to_do = FALSE;
    $this_file = '';
    foreach ($li_children as $elem) {
      if ($elem->tagName == 'a') {
        // Add this file to the parents array.
        $this_file = $elem->getAttribute('href');
        if ($link_prefix) {
          $this_file = str_replace($link_prefix, '', $this_file);
        }
        $parents[$this_file] = array('file' => $parent, 'weight' => $weight);
      }
      elseif ($elem->tagName == 'ul') {
        $ul_to_do = $elem;
      }
      // Parse the next level deep in the table of contents, and add it to
      // the parents array.
      if ($ul_to_do && $this_file) {
        $parents = _asciidoc_display_calculate_parents($ul_to_do, $this_file, $link_prefix, $parents);
      }
    }
  }

  return $parents;
}

/**
 * Renders a DOMElement into an HTML string.
 */
function _asciidoc_display_dom_element_to_html($elem) {
  if (!$elem) {
    return '';
  }

  $doc = new \DOMDocument('1.0', 'UTF-8');
  $cloned = $elem->cloneNode(TRUE);
  $doc->appendChild($doc->importNode($cloned, TRUE));
  $html = @$doc->saveHTML();
  if (!$html) {
    return '';
  }

  return $html;
}

/**
 * Adds a prefix to internal URLs.
 *
 * @param DomDocument $dom
 *   Document to traverse.
 * @param string $tag
 *   Tag name to look for, normally either 'a' or 'img'.
 * @param string $attribute
 *   Attribute containing the URL, 'href' or 'src'.
 * @param string $prefix
 *   Prefix to put on internal links
 */
function _asciidoc_display_prefix_links($dom, $tag, $attribute, $prefix) {
  if (!$prefix) {
    return;
  }

  $links = $dom->getElementsByTagName($tag);
  foreach ($links as $link) {
    $href = $link->getAttribute($attribute);
    if (!$href) {
      continue;
    }
    // Do not prefix any links with full URLs, or that already start with
    // the prefix.
    if (strpos($href, ':') !== FALSE || strpos($href, $prefix) === 0) {
      continue;
    }
    $link->setAttribute($attribute, $prefix . $href);
  }
}

/**
 * Sets a DOM node to have class "active", and parents to "active_trail".
 *
 * @param \DOMNode $link
 *   A link that is deemed to be active.
 */
function _asciidoc_display_link_set_active($link) {
  // Set the link itself to have class "active".
  $class = $link->getAttribute('class');
  $classes = array_filter(explode(' ', $class));
  if (!in_array('active', $classes)) {
    $classes[] = 'active';
  }
  $link->setAttribute('class', implode(' ', $classes));

  // Set all LI elements that are parents of this to have class "active-trail".
  $node = $link;
  while ($node = $node->parentNode) {
    if (is_a($node, 'DOMElement') && ($node->tagName == 'li' || $node->tagName == 'ul')) {
      $class = $node->getAttribute('class');
      $classes = array_filter(explode(' ', $class));
      if (!in_array('active-trail', $classes)) {
        $classes[] = 'active-trail';
      }
      $node->setAttribute('class', implode(' ', $classes));
    }
  }
}

/**
 * Finds and highlights PHP code.
 *
 * Note: Because of how DOM elements work, you need to call this several
 * times, because when you replace nodes the array gets screwed up.
 *
 * @param \DOMNode $node
 *   DOM node object to highlight.
 * @param \DOMDocument $doc
 *   DOM document object this is in.
 */
function _asciidoc_display_highlight_xml_once($node, $doc) {
  $pres = $node->getElementsByTagName('pre');
  foreach ($pres as $pre) {
    $class = $pre->getAttribute('class');
    $classes = array_filter(explode(' ', $class));
    if (in_array('programlisting-php', $classes)) {
      $newcode = _asciidoc_display_highlight_code($pre->nodeValue);
      $newdoc = new \DOMDocument('1.0', 'UTF-8');
      $newdoc->loadHTML($newcode);
      $el = $newdoc->getElementsByTagName('code')->item(0);
      $newnode = $doc->importNode($el, TRUE);
      // This is the part that screws up the array of $pres. I tried keeping
      // track of what needs to be replaced, and doing it all at the end,
      // but the act of doing replaceChild changes the parent and all of
      // its children, so that doesn't work well. You just need to call this
      // function several times to make sure you get everything.
      $pre->parentNode->replaceChild($newnode, $pre);
    }
  }
}

/**
 * Highlights PHP code.
 *
 * @param string $code
 *   Code to highlight.
 *
 * @return string
 *   Highlighted code.
 */
function _asciidoc_display_highlight_code($code) {
  // Get rid of the &gt; and &lt; entities.
  $code = str_replace(array('&gt;', '&lt;'), array('>', '<'), trim($code));

  // Add a <?php tag to the front.
  $strip = FALSE;
  if (strpos('<?php', $code) !== 0) {
    $code = "<?php " . $code;
    $strip = TRUE;
  }

  // Highlight using native PHP code highlighter.
  $code = highlight_string($code, TRUE);

  // Strip off the initial <?php tag.
  if ($strip) {
    $code = implode('', explode('&lt;?php&nbsp;', $code, 2));
  }

  return $code;
}
